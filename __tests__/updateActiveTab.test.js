/* globals global */
import {prepareTabs, rewireWith} from './_helpers.js';

let updateActiveTabRewireAPI, testFunctions;

beforeAll(() => {
  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.isVersion = commonRewireAPI.__GetDependency__('isVersion');

  const titlesRewireAPI = require('../addon/background_scripts/titles.js');
  global.tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');

  updateActiveTabRewireAPI = require('../addon/background_scripts/updateActiveTab.js');
  testFunctions = rewireWith.bind(null, updateActiveTabRewireAPI);
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('updateActiveTab', () => {
  it('updateActiveTab should call updateIcon', () => {
    const updateActiveTab = updateActiveTabRewireAPI.__GetDependency__('updateActiveTab');
    const tabs = prepareTabs(3);
    return testFunctions('updateIcon', async updateIcon => {
      await updateActiveTab({opener: tabs[2]});
      expect(updateIcon).toBeCalledTimes(1);
      expect(updateIcon).toBeCalledWith(tabs[0], tabs[2]);
    });
  });

  it('onUrlChanged should call updateIcon only when active tab url changed', () => {
    const onUrlChanged = updateActiveTabRewireAPI.__GetDependency__('onUrlChanged');
    return testFunctions('updateIcon', updateIcon => {
      const tabs = prepareTabs(1);
      tabs[0].active = true;
      onUrlChanged(1, {state: 'loading'}, tabs[0]);
      expect(updateIcon).toBeCalledTimes(0);

      tabs[0].active = false;
      onUrlChanged(1, {state: 'loading', url: 'some-url'}, tabs[0]);
      expect(updateIcon).toBeCalledTimes(0);

      tabs[0].active = true;
      onUrlChanged(1, {state: 'loading', url: 'some-url'}, tabs[0]);
      expect(updateIcon).toBeCalledTimes(1);
      expect(updateIcon).toBeCalledWith(tabs[0]);
    });
  });

  it('updateIcon should update browserAction disabled state', () => {
    const updateIcon = updateActiveTabRewireAPI.__GetDependency__('updateIcon');
    const tabs = prepareTabs(['http:something', 'about:something']);
    // call updateIcon with opener - tab 1 url isExcludedUrl
    updateIcon(tabs[0], tabs[1]);
    expect(browser.browserAction.disable).toBeCalledTimes(1);
    expect(browser.browserAction.disable).toBeCalledWith(1);

    // call updateIcon with opener - tab 0 url is ok
    updateIcon(tabs[1], tabs[0]);
    expect(browser.browserAction.enable).toBeCalledTimes(1);
    expect(browser.browserAction.enable).toBeCalledWith(2);

    jest.clearAllMocks();

    // call updateIcon without opener - tab 1 url isExcludedUrl
    updateIcon(tabs[1]);
    expect(browser.browserAction.disable).toBeCalledTimes(1);
    expect(browser.browserAction.disable).toBeCalledWith(2);

    // call updateIcon with opener - tab 0 url is ok
    updateIcon(tabs[0]);
    expect(browser.browserAction.enable).toBeCalledTimes(1);
    expect(browser.browserAction.enable).toBeCalledWith(1);
  });

  it('updateIcon should update browserAction icon', () => {
    const updateIcon = updateActiveTabRewireAPI.__GetDependency__('updateIcon');
    const tabs = prepareTabs(['http:something', 'about:something', 'http:something.else']);

    const pathBlue = '../icons/rename_icon_blue.svg';
    const pathBlack = '../icons/rename_icon.svg';

    // tab without user title should be black
    updateIcon(tabs[0]);
    expect(browser.browserAction.setIcon).lastCalledWith({
      path: pathBlack,
      tabId: 1,
    });

    // tab with excluded url should be black
    updateIcon(tabs[1]);
    expect(browser.browserAction.setIcon).lastCalledWith({
      path: pathBlack,
      tabId: 2,
    });

    // tab with user title should be blue
    global.tabsTitles.saveTitles(tabs[2], {permanently: true, title: 'title'});
    updateIcon(tabs[2]);
    expect(browser.browserAction.setIcon).lastCalledWith({
      path: pathBlue,
      tabId: 3,
    });
  });
});

