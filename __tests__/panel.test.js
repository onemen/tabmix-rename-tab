/**
 * @jest-environment jsdom
 */

/* eslint jest/expect-expect: ["error", { "assertFunctionNames": ["expect", "openRenameTabPanel"] }] */

import {openRenameTabPanel} from './panel_helpers.js';

// Each test have an initial state changes and expected state
// in the panel user can change title and permanently checkbox, and click buttons
describe('renameTab panel', () => {
  it('tabsTitles should contains title for 1st page,  fixedTitle=false', async() => {
    await openRenameTabPanel({
      titleFromBookmark: false,
      page: 'page1',
      start: 'null',
      changes: {
        newTitle: 'rename1',
        checked: false,
        buttonId: 'updateButton',
      },
      expected: 'expected3',
    });
  });

  it('tabsTitles should contains fixedTitle,  fixedTitle=true', async() => {
    await openRenameTabPanel({
      titleFromBookmark: false,
      page: 'page2',
      start: 'expected3',
      changes: {
        newTitle: 'rename2',
        checked: true,
        buttonId: 'updateButton',
      },
      expected: 'expected2',
    });
  });

  it('tabsTitles should contains user title for page 1', async() => {
    await openRenameTabPanel({
      titleFromBookmark: false,
      page: 'page1',
      start: 'expected2',
      changes: {
        newTitle: 'rename1',
        checked: false,
        buttonId: 'updateButton',
      },
      expected: 'expected3',
    });
  });

  it('reset should clear data for a page 2, fixedTitle=false', async() => {
    await openRenameTabPanel({
      titleFromBookmark: false,
      page: 'page2',
      start: 'expected4',
      changes: {
        newTitle: 'rename1',
        checked: false,
        buttonId: 'resetButton',
      },
      expected: 'expected3',
    });
  });

  it('reset should clear all data, fixedTitle=true', async() => {
    await openRenameTabPanel({
      titleFromBookmark: false,
      page: 'page2',
      start: 'expected2',
      changes: {
        newTitle: 'rename1',
        checked: false,
        buttonId: 'resetButton',
      },
      expected: 'null',
    });
  });

  it('keypress should close the panel', async() => {
    await openRenameTabPanel({
      page: 'page1',
      start: 'null',
      test: 'keypress',
    });
  });

  it('panel should not close when error occur', async() => {
    await openRenameTabPanel({
      page: 'page1',
      start: 'null',
      test: 'error',
    });
  });

  it('panel should not close when title field is empty', async() => {
    await openRenameTabPanel({
      page: 'page1',
      start: 'null',
      changes: {newTitle: ''},
      test: 'keypress',
    });
  });
});
