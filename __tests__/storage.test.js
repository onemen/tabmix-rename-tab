import {getPreferences, resetPreferences, rewireWith} from './_helpers.js';
// eslint-disable-next-line import/named
import {__RewireAPI__ as storageRewireAPI} from '../addon/background_scripts/storage.js';

const testFunctions = rewireWith.bind(null, storageRewireAPI);

let preferences;

beforeAll(() => {
  preferences = getPreferences();
  preferences.pref_number = 10;
  preferences.pref_boolean = false;
});

afterAll(() => {
  resetPreferences();
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('storage updatePref', () => {
  const updatePref = storageRewireAPI.__GetDependency__('updatePref');

  it('should return false', () => {
    // newValue undefined
    expect(updatePref('pref_number')).toBe(false);
    // newValue undefined
    expect(updatePref('pref_not_exist', 100)).toBe(false);
  });

  it('should return true', () => {
    expect(updatePref('pref_number', 100)).toBe(true);
    expect(updatePref('pref_number', true)).toBe(true);
    expect(updatePref('pref_boolean', false)).toBe(true);
    expect(updatePref('pref_boolean', 11)).toBe(true);
  });

  it('should update preference by initial type', () => {
    updatePref('pref_number', 100);
    expect(preferences.pref_number).toEqual(100);
    updatePref('pref_number', true);
    expect(preferences.pref_number).toEqual(1);
    updatePref('pref_number', false);
    expect(preferences.pref_number).toEqual(0);
    updatePref('pref_number', '-100');
    expect(preferences.pref_number).toEqual(-100);
    updatePref('pref_number', 'true');
    // NaN
    expect(preferences.pref_number).toEqual(Number('xxx'));

    updatePref('pref_boolean', 100);
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', -100);
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', '100');
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', true);
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', false);
    expect(preferences.pref_boolean).toEqual(false);
  });
});

describe('storage', () => {
  const onStorageChanged = storageRewireAPI.__GetDependency__('onStorageChanged');

  it('onStorageChanged should not call notifyListeners', () => {
    preferences.onChanged.notifyListeners = jest.fn();
    onStorageChanged([], 'area_not_local');
    expect(preferences.onChanged.notifyListeners).toBeCalledTimes(0);
  });

  it('onStorageChanged should call notifyListeners', () => {
    const changes = {
      save_with_sessions: {oldValue: true, newValue: false},
      show_in_context_menu: {oldValue: true, newValue: true},
      pref_number: {oldValue: 10, newValue: 100},
      pref_boolean: {oldValue: false, newValue: false},
    };

    preferences.onChanged.notifyListeners = jest.fn();
    const original = storageRewireAPI.__GetDependency__('updatePref');
    const mockUpdatePref = {name: 'updatePref', fn: jest.fn((...args) => original(...args))};
    return testFunctions([mockUpdatePref], ([updatePref]) => {
      onStorageChanged(changes, 'local');

      expect(updatePref).toBeCalledTimes(2);
      expect(updatePref).toBeCalledWith('save_with_sessions', false);
      expect(updatePref).toBeCalledWith('pref_number', 100);

      expect(preferences.onChanged.notifyListeners).toBeCalledTimes(1);
      expect(preferences.onChanged.notifyListeners).toBeCalledWith(['save_with_sessions', 'pref_number']);
    });
  });

  it('updateAllPrefs should call updatePref for each pref and return list', () => {
    const updateAllPrefs = storageRewireAPI.__GetDependency__('updateAllPrefs');

    const original = storageRewireAPI.__GetDependency__('updatePref');
    const mockUpdatePref = {name: 'updatePref', fn: jest.fn((...args) => original(...args))};
    return testFunctions([mockUpdatePref], ([updatePref]) => {
      const result = updateAllPrefs({
        save_with_sessions: false,
        pref_number: 1000,
        show_in_context_menu: true,
        pref_boolean: true,
        title_from_bookmark: false,
      });

      expect(updatePref).toBeCalledTimes(5);
      expect(result).toEqual([
        'save_with_sessions',
        'show_in_context_menu',
        'title_from_bookmark',
        'pref_number',
        'pref_boolean',
      ]);

      expect(preferences.save_with_sessions).toEqual(false);
      expect(preferences.pref_number).toEqual(1000);
      expect(preferences.show_in_context_menu).toEqual(true);
      expect(preferences.pref_boolean).toEqual(true);
      expect(preferences.title_from_bookmark).toEqual(false);
    });
  });
});
