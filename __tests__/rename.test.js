/* globals global */
import {cloneObject, getPreferences, resetPreferences} from './_helpers.js';
import {data} from './fixtures/data.js';
import {__RewireAPI__ as titlesRewireAPI} from '../addon/background_scripts/titles.js';

const tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');
let contextMenuRewireAPI;
let preferences;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.debounce = commonRewireAPI.__GetDependency__('debounce');
  global.getDocumentTitle = commonRewireAPI.__GetDependency__('getDocumentTitle');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');
  global.toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');
  global.isVersion = commonRewireAPI.__GetDependency__('isVersion');
  global.tabsTitles = tabsTitles;

  const updateActiveTabRewireAPI = require('../addon/background_scripts/updateActiveTab.js');
  global.updateActiveTab = updateActiveTabRewireAPI.__GetDependency__('updateActiveTab');
  global.updateIcon = updateActiveTabRewireAPI.__GetDependency__('updateIcon');

  const bookmarksRewireAPI = require('../addon/background_scripts/bookmarks.js');
  global.getBookmark = bookmarksRewireAPI.__GetDependency__('getBookmark');

  contextMenuRewireAPI = require('../addon/background_scripts/contextMenus.js');
});

afterAll(() => {
  resetPreferences();
});

// we add toggleListeners function to tabsTitles in tabUpdated.js
// as long as we don't load it here we need to add a mock to this function
tabsTitles.toggleListeners = jest.fn();

describe('tabsTitles.saveTitles', () => {
  it('tabsTitles should be an empty object', () => {
    expect(typeof tabsTitles).toBe('object');
  });
  it('tabsTitles data should be an empty on start', () => {
    expect(Object.keys(tabsTitles.data)).toHaveLength(0);
  });
  it('reset title should not throws an error when there is no data', () => {
    tabsTitles.saveTitles(data.page1);
    expect.anything();
  });
  it('tabsTitles should contains fixedTitle', () => {
    // 1st page
    const {page1, rename1, expected1} = Object.assign({}, data);
    tabsTitles.saveTitles(page1, rename1);
    expect(tabsTitles.getData(1)).toEqual(expected1);
  });
  it('tabsTitles should contains user title for page 1', () => {
    // 1nd page with permanently false
    const {page1, rename1, expected3} = Object.assign({}, data);
    const renameData = Object.assign({}, rename1);
    renameData.permanently = false;
    tabsTitles.saveTitles(page1, renameData);
    expect(tabsTitles.getData(1)).toEqual(expected3);
  });
  it('tabsTitles should contains user titles for page 1 and page 2', () => {
    // 2nd page with permanently false
    const {page2, rename2, expected4} = Object.assign({}, data);
    const renameData = Object.assign({}, rename2);
    renameData.permanently = false;
    tabsTitles.saveTitles(page2, renameData);
    expect(tabsTitles.getData(1)).toEqual(expected4);
  });
  it('after resetting page 2 tabsTitles should contains user title for page 1', () => {
    // reset title for page 2
    const {page2, rename2, expected3} = Object.assign({}, data);
    const renameData = Object.assign({}, rename2);
    renameData.title = '';
    tabsTitles.saveTitles(page2, renameData);
    expect(tabsTitles.getData(1)).toEqual(expected3);
  });
  it('Reset the last url should remove all data for tab', () => {
    // reset title for page 2
    const {page1, rename1} = Object.assign({}, data);
    const renameData = Object.assign({}, rename1);
    renameData.title = '';
    tabsTitles.saveTitles(page1, renameData);
    expect(tabsTitles.getData(1)).toEqual(data.empty);
  });
});

describe('test addPanelListeners - tab with 1 page', () => {
  const tab = Object.assign({}, data.page1);

  const baseMessage = {
    image: tab.favIconUrl,
    permanently: false,
    documentTitle: 'documentTitle',
    currentUserTitle: '',
    modified: false,
    permanentlyChecked: true,
  };

  const messageToPanel = Object.assign({
    type: 'init_state_for_panel',
    title: tab.title,
  }, baseMessage);

  const messageFromPanel = Object.assign({
    type: 'rename_panel_done',
    title: data.rename1.title,
    permanentlyChecked: true,
  }, baseMessage);

  const messageFromPanelWithReset = Object.assign({}, messageFromPanel);
  messageFromPanelWithReset.reset = true;

  let asyncOnMessage, checkRespondOnPanelDone;
  beforeAll(() => {
    // rewire contextMenu functions
    const addPanelListeners = contextMenuRewireAPI.__GetDependency__('addPanelListeners');
    checkRespondOnPanelDone = contextMenuRewireAPI.__GetDependency__('checkRespondOnPanelDone');

    // our listener return promise
    asyncOnMessage = addPanelListeners(tab);
  });

  it('tabsTitles data should be empty on start', () => {
    Object.keys(tabsTitles.data).forEach(id => delete tabsTitles.data[id]);
    expect(Object.keys(tabsTitles.data)).toHaveLength(0);
  });
  it('should get proper response from message listener, when bookmark pref is off', async() => {
    preferences.title_from_bookmark = false;
    expect.assertions(1);
    const panelOpened = {type: 'rename_panel_opened'};
    await expect(asyncOnMessage(panelOpened)).resolves.toEqual(messageToPanel);
  });
  it('should get proper response from message listener, when bookmark pref is on', async() => {
    preferences.title_from_bookmark = true;
    expect.assertions(1);
    const panelOpened = {type: 'rename_panel_opened'};
    const message = cloneObject(messageToPanel, {documentTitle: 'Home Page'});
    await expect(asyncOnMessage(panelOpened)).resolves.toEqual(message);
  });
  it('mock of tabs.sendMessage should return expected result', async() => {
    expect(await browser.tabs.sendMessage(999, {type: 'change_doc_title', title: 'xxx'})).toEqual('xxx');
    expect(await browser.tabs.sendMessage(999, {type: 'change_doc_title', title: ''})).toEqual('reset_doc_title');
    expect(await browser.tabs.sendMessage(999, {type: 'reset_doc_title'})).toEqual('reset_doc_title');
  });
  it('should call executeScript to change document.title', async() => {
    await asyncOnMessage(messageFromPanel);

    const args = [tab.id, {type: 'change_doc_title', title: messageFromPanel.title}];
    expect(browser.tabs.sendMessage).toBeCalledWith(...args);
  });
  it('checkRespondOnPanelDone should return expected result', () => {
    const result = checkRespondOnPanelDone(tab, messageFromPanel);
    expect(result).toEqual({
      tab,
      stateChanged: true,
      title: data.rename1.title,
      permanently: true,
    });
  });
  it('should save title data', () => {
    expect(tabsTitles.getData(1)).toEqual(data.expected1);
  });
  it('reset title should remove all data for a tab', async() => {
    expect.assertions(1);
    await asyncOnMessage(messageFromPanelWithReset);
    expect(tabsTitles.getData(1)).toEqual(data.empty);
  });
  it('checkRespondOnPanelDone should return expected result when reset is true', () => {
    const result = checkRespondOnPanelDone(tab, messageFromPanelWithReset);
    expect(result).toEqual({
      tab,
      title: data.rename1.title,
      reset: true,
    });
  });
  it('checkRespondOnPanelDone should return reset for equal titles', () => {
    // title did not changed and permanently is false
    const respond = {
      title: '   tab title  ',
      documentTitle: 'tab title',
      permanentlyChecked: false,
    };
    const result = checkRespondOnPanelDone(tab, respond);
    expect(result.reset).toBe(true);
  });
  it('checkRespondOnPanelDone should return reset', () => {
    const respond = {
      title: 'xxx',
      documentTitle: 'yyy',
      reset: true,
    };
    const result = checkRespondOnPanelDone(tab, respond);
    expect(result.reset).toBe(true);
  });
  it('checkRespondOnPanelDone should return stateChanged', () => {
    const respond = {
      title: '   tab title  ',
      currentUserTitle: 'xxx',
      permanently: true,
      permanentlyChecked: false,
    };
    const result1 = checkRespondOnPanelDone(tab, respond);
    expect(result1.stateChanged).toBe(true);

    respond.title = respond.currentUserTitle;
    const result2 = checkRespondOnPanelDone(tab, respond);
    expect(result2.stateChanged).toBe(true);
  });
  it('checkRespondOnPanelDone should not return stateChanged', () => {
    const respond = {
      title: '   tab title  ',
      currentUserTitle: 'title',
      permanently: false,
      permanentlyChecked: false,
    };
    const result = checkRespondOnPanelDone(tab, respond);
    expect(result.stateChanged).toBe(true);
  });
  it('newTitle should fallback to tab title when title is empty', () => {
    const respond1 = {title: '', currentUserTitle: 'user-title'};
    const result1 = checkRespondOnPanelDone({title: 'title-1'}, respond1);
    expect(result1.title).toEqual('user-title');

    const respond2 = {title: ''};
    const result2 = checkRespondOnPanelDone({title: 'title-1'}, respond2);
    expect(result2.title).toEqual('title-1');
  });
  it('should remove browser.runtime listener when the panel is closing', async() => {
    browser.runtime.onMessage.hasListener = jest.fn(arg => arg === asyncOnMessage);
    await asyncOnMessage({type: 'rename_panel_closed'});
    expect(browser.runtime.onMessage.hasListener).toBeCalledTimes(1);
    expect(browser.runtime.onMessage.removeListener).toBeCalledTimes(1);
  });
});

describe('test addPanelListeners - tab with 2 pages', () => {
  const page1 = Object.assign({}, data.page1);
  const page2 = Object.assign({}, data.page2);

  const baseMessage = {
    image: page1.favIconUrl,
    permanently: false,
    currentUserTitle: '',
    modified: false,
    permanentlyChecked: true,
  };

  const updateTitle = (page, title, permanentlyChecked) => {
    const addPanelListeners = contextMenuRewireAPI.__GetDependency__('addPanelListeners');
    const asyncOnMessage = addPanelListeners(page);
    const clonedData = Object.assign({}, baseMessage);
    const messageFromPanel = Object.assign(clonedData, {
      type: 'rename_panel_done',
      title,
      documentTitle: page.title,
      permanentlyChecked,
    });
    return asyncOnMessage(messageFromPanel);
  };

  it('tabsTitles data should be empty on start', () => {
    Object.keys(tabsTitles.data).forEach(id => delete tabsTitles.data[id]);
    expect(Object.keys(tabsTitles.data)).toHaveLength(0);
  });
  it('tabsTitles should contains title for 1st page', async() => {
    // rename page1, fixedTitle=false
    expect.assertions(1);
    await updateTitle(page1, data.rename1.title, false);
    expect(tabsTitles.getData(1)).toEqual(data.expected3);
  });
  it('tabsTitles should contains fixedTitle', async() => {
    // rename page2, fixedTitle=true
    expect.assertions(1);
    await updateTitle(page2, data.rename2.title, true);
    expect(tabsTitles.getData(1)).toEqual(data.expected2);
  });
  it('tabsTitles should contains user titles for page 1 and 2', async() => {
    // rename page1, fixedTitle=false
    expect.assertions(1);
    await updateTitle(page1, data.rename1.title, false);
    await updateTitle(page2, data.rename2.title, false);
    expect(tabsTitles.getData(1)).toEqual(data.expected4);
  });
});
