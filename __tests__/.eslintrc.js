module.exports = {
  "extends": [
    "plugin:jest/recommended",
  ],
  "parserOptions": {
    "sourceType": "module"
  },
  "rules": {
    "jest/consistent-test-it": "error"
  }
};
