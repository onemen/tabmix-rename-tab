require('jest-webextension-mock');

const messageHelper = require('./message_helpers');

// add missing functions to the mock object
if (!browser.contextMenus) {
  browser.contextMenus = {
    onClicked: new messageHelper(),
    onShown: new messageHelper(),
    create: jest.fn(),
    remove: jest.fn(),
    update: jest.fn(),
    refresh: jest.fn(),
  };
}

browser.browserAction = {
  ...browser.browserAction,
  onClicked: new messageHelper(),
  openPopup: jest.fn(),
};

browser.tabs = {
  ...browser.tabs,
  captureTab: jest.fn(() => Promise.resolve()),
  onActivated: new messageHelper(),
  onAttached: new messageHelper(),
  onCreated: new messageHelper(),
  onRemoved: new messageHelper(),
  onUpdated: new messageHelper(),
  query: jest.fn(() => Promise.resolve([])),
  sendMessage: jest.fn((id, message) => {
    if (message.type == 'get_original_title') {
      return Promise.resolve('documentTitle');
    }
    return Promise.resolve(message.title || 'reset_doc_title');
  }),
};

// mock browser.bookmarks
browser.bookmarks = {
  onCreated: new messageHelper(),
  onChanged: new messageHelper(),
  onRemoved: new messageHelper(),
  search: jest.fn(({url}) => {
    const bookmarks = {
      'http://tabmixplus.org': {
        id: 'aaa',
        title: 'Home Page',
      },
      'http://tabmixplus.org/forum': {
        id: 'bbb',
        title: 'Forum',
      },
      'https://www.babelzilla.org/': {
        id: 'ccc',
        title: 'Provide localizations',
      },
    };
    const bookmark = bookmarks[url] || '';
    return Promise.resolve(bookmark ? [bookmark] : []);
  }),
};

browser.i18n = {getMessage: jest.fn()};

const runtimeMessage = new messageHelper();
browser.runtime.sendMessage = runtimeMessage.command;
browser.runtime.onMessage = runtimeMessage;
browser.runtime.getBrowserInfo = jest.fn(() => ({version: '60.0.0'}));

browser.sessions = {
  getTabValue: jest.fn(() => Promise.resolve()),
  removeTabValue: jest.fn(() => Promise.resolve()),
  setTabValue: jest.fn(() => Promise.resolve()),
};
