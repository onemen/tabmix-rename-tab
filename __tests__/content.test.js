/**
 * @jest-environment jsdom
 */
/* globals global */

import {rewireWith} from './_helpers.js';

const commonRewireAPI = require('../addon/background_scripts/common.js');
const setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');

const PATH_TO_CONTENT = '../addon/content_scripts/content.js';

const ORIGINAL = 'original test title';

// According to MDN documents "The message will be received in the content
// scripts by any listeners to the runtime.onMessage event"
// We will forward the message to runtime.onMessage
browser.tabs.sendMessage = jest.fn((id, message) => {
  return browser.runtime.sendMessage(message);
});

beforeAll(() => {
  document.title = ORIGINAL;
});

describe('content script', () => {
  const contentRewireAPI = require(PATH_TO_CONTENT);
  const testFunctions = rewireWith.bind(null, contentRewireAPI);

  const get = arg => contentRewireAPI.__GetDependency__(arg);
  const onMessage = contentRewireAPI.__GetDependency__('onMessage');

  function getOriginalTitle() {
    return browser.tabs.sendMessage(1, {type: 'get_original_title'});
  }

  it('contentScript should save a copy of document.title after DOMContentLoaded', async () => {
    document.dispatchEvent(new Event('DOMContentLoaded'));
    document.title = 'xxxxxxxx';
    await expect(getOriginalTitle()).resolves.toBe(ORIGINAL);
  });

  it('change_doc_title message should change document title', async () => {
    const result = await browser.tabs.sendMessage(1, {type: 'change_doc_title', title: 'new title'});
    expect(result).toBe('new title');
    expect(document.title).toBe('new title');
  });

  it('change_doc_title message should save bookmark id when argument passed', async () => {
    document.title = 'xxxxxxxx';
    const result = await browser.tabs.sendMessage(1, {
      type: 'change_doc_title',
      title: 'bookmark title',
      bookmarkId: '1001',
    });
    expect(result).toBe('bookmark title');
    const bookmarkId = await browser.tabs.sendMessage(1, {type: 'get_bookmark_id'});
    expect(bookmarkId).toBe('1001');
  });

  it('get_extension_data message should return all data', async () => {
    const data = await browser.tabs.sendMessage(1, {type: 'get_extension_data'});
    expect(data).toEqual({
      originalTitle: ORIGINAL,
      title: 'bookmark title',
      bookmarkId: '1001',
    });
  });

  it('setDocumentTitle should set document.title and bookmarkId', async () => {
    document.title = 'xxxxxxxx';
    await setDocumentTitle(1, 'xxxxxxxx', {title: 'bookmark title', id: '999'});
    const data = await browser.tabs.sendMessage(1, {type: 'get_extension_data'});
    expect(data).toEqual({
      originalTitle: ORIGINAL,
      title: 'bookmark title',
      bookmarkId: '999',
    });
  });

  it('get_original_title message should get original title', async () => {
    document.title = 'xxxxxxxx';
    await expect(getOriginalTitle()).resolves.toBe(ORIGINAL);
    expect(document.title).toBe('xxxxxxxx');
  });

  it('reset_doc_title message should reset document title', async () => {
    document.title = 'xxxxxxxx';
    const result = await browser.tabs.sendMessage(1, {type: 'reset_doc_title'});
    expect(result).toBe('reset_doc_title');
    expect(document.title).toBe(ORIGINAL);
    await expect(getOriginalTitle()).resolves.toBe(ORIGINAL);
  });

  it('reset_doc_title message should delete bookmarkId', async () => {
    const data = await browser.tabs.sendMessage(1, {type: 'get_extension_data'});
    // no bookmarkId expected
    expect(data).toEqual({
      originalTitle: ORIGINAL,
      title: ORIGINAL,
    });
  });

  it('change_doc_title message without a title should reset document title', async () => {
    document.title = 'xxxxxxxx';
    const result = await browser.tabs.sendMessage(1, {type: 'change_doc_title', title: ''});
    expect(result).toBe('reset_doc_title');
    expect(document.title).toBe(ORIGINAL);
    await expect(getOriginalTitle()).resolves.toBe(ORIGINAL);
  });

  it('should call delayedRespond when document.title is not ready', () => {
    // save current propertyDescriptor
    const titleDescriptor = Object.getOwnPropertyDescriptor(Document.prototype, 'title');
    Object.defineProperty(document, 'title', {
      get: () => {},
      set: () => {},
      enumerable: true,
      configurable: true,
    });

    return testFunctions('delayedRespond', async delayedRespond => {
      await browser.tabs.sendMessage(1, {type: 'change_doc_title', title: 'new title'});
      expect(delayedRespond).toBeCalledTimes(1);
      // restore propertyDescriptor
      Object.defineProperty(document, 'title', titleDescriptor);
    });
  });

  it('delayedRespond should return lastTitle', async () => {
    contentRewireAPI.__Rewire__('lastTitle', 'last title');
    const delayedRespond = contentRewireAPI.__GetDependency__('delayedRespond');
    document.dispatchEvent(new Event('DOMContentLoaded'));
    const result = await delayedRespond();
    expect(result).toEqual('last title');
  });

  it('onMessage should return Promise.reject', () => {
    const originalConsoleError = global.console.error;
    global.console.error = jest.fn();

    const error = new Error('Unexpected error 1234');
    const mockUpdateTitle = {
      name: 'updateTitle',
      fn: jest.fn(() => {throw error;}),
    };
    return testFunctions(mockUpdateTitle, async () => {
      await expect(onMessage()).rejects.toEqual(error);

      expect(global.console.error).toBeCalledTimes(1);
      expect(global.console.error).toBeCalledWith(error, `url: ${document.URL}`);
      global.console.error = originalConsoleError;
    });
  });

  it('should handle change_doc_title', async () => {
    document.title = ORIGINAL;
    contentRewireAPI.__Rewire__('lastTitle', '');
    contentRewireAPI.__Rewire__('data', {});

    const result = await onMessage({type: 'change_doc_title', title: 'title-101', bookmarkId: '2002'});
    expect(result).toEqual('title-101');
    expect(document.title).toEqual('title-101');
    expect(document.title).toEqual('title-101');
    expect(get('data')).toEqual({bookmarkId: '2002', originalTitle: ORIGINAL});
    expect(get('lastTitle')).toEqual('title-101');
  });

  it('should handle reset_doc_title', async () => {
    document.title = 'title-202';
    contentRewireAPI.__Rewire__('lastTitle', 'title-202');
    contentRewireAPI.__Rewire__('data', {bookmarkId: '1001', originalTitle: ORIGINAL});

    const result = await onMessage({type: 'reset_doc_title'});
    expect(result).toEqual('reset_doc_title');
    expect(document.title).toEqual(ORIGINAL);
    expect(get('data')).toEqual({originalTitle: ORIGINAL});
    expect(get('lastTitle')).toEqual(ORIGINAL);
  });

  it('should handle reset bookmarkId', async () => {
    document.title = ORIGINAL;
    contentRewireAPI.__Rewire__('lastTitle', '');
    contentRewireAPI.__Rewire__('data', {bookmarkId: '3003', originalTitle: ORIGINAL});

    const result = await onMessage({type: 'reset_doc_title'});
    expect(result).toEqual('reset_doc_title');
    expect(document.title).toEqual(ORIGINAL);
    expect(get('data')).toEqual({originalTitle: ORIGINAL});
    expect(get('lastTitle')).toEqual(ORIGINAL);
  });

  it('get_bookmark_id should return string', async () => {
    contentRewireAPI.__Rewire__('data', {bookmarkId: '3003', originalTitle: ORIGINAL});
    const result1 = await onMessage({type: 'get_bookmark_id'});
    expect(result1).toEqual('3003');
    expect(typeof result1).toEqual('string');

    contentRewireAPI.__Rewire__('data', {originalTitle: ORIGINAL});
    const result2 = await onMessage({type: 'get_bookmark_id'});
    expect(result2).toEqual('');
    expect(typeof result2).toEqual('string');
  });
});

describe('setDocumentTitle', () => {
  const originalConsoleError = global.console.error;
  global.console.error = jest.fn();

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    global.console.error = originalConsoleError;
  });

  it('setDocumentTitle should handel error', async () => {
    const error = new Error('Unexpected result "zzzz" instead of "new Title"');
    browser.tabs.sendMessage = jest.fn(() => {
      return Promise.resolve('zzzz');
    });
    const result = await setDocumentTitle(1, 'xxxxxxxx', 'new Title', {data: 'some data'});
    expect(result).toEqual({
      success: false,
      error,
    });
    expect(global.console.error).toBeCalledTimes(1);
    expect(global.console.error).toBeCalledWith(error, {data: 'some data'});
  });

  it('setDocumentTitle should handel reject', async () => {
    const error = new Error('some unexpected error');
    browser.tabs.sendMessage = jest.fn(() => {
      return Promise.reject(error);
    });
    const result = await setDocumentTitle(1, 'xxxxxxxx', 'new Title', {data: 'some data'});
    expect(result).toEqual({
      success: false,
      error,
    });
    expect(global.console.error).toBeCalledTimes(1);
    expect(global.console.error).toBeCalledWith(error, {data: 'some data'});
  });

  it('setDocumentTitle should not log error to the console', async () => {
    browser.tabs.sendMessage = jest.fn(() => {
      throw new Error('COULD NOT ESTABLISH CONNECTION');
    });
    const result = await setDocumentTitle(1, 'xxxxxxxx', 'new Title');
    expect(result).toEqual({
      success: false,
      error: new Error('COULD NOT ESTABLISH CONNECTION'),
    });
    expect(global.console.error).toBeCalledTimes(0);
  });
});
