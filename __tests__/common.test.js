const commonRewireAPI = require('../addon/background_scripts/common.js');
const debounce = commonRewireAPI.__GetDependency__('debounce');
const isVersion = commonRewireAPI.__GetDependency__('isVersion');
const toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');

const messageHelper = require('./message_helpers');

const clickMessageObj = new messageHelper();
const removeMessageObj = new messageHelper();
const testObj = {
  sendClick: clickMessageObj.command,
  onClicked: clickMessageObj,
  sendRemove: removeMessageObj.command,
  onRemoved: removeMessageObj,
};

const onClicked = jest.fn();
const onRemoved = jest.fn();
const onClickedCallback = jest.fn();

function onTestClicked() {
  onClicked();
}

function onTestRemoved() {
  onRemoved();
}

function callbackTestFunction(fnName) {
  switch (fnName) {
    case 'onTestClicked':
      onClickedCallback(fnName);
      break;
    case 'onBookmarkCreated':
      onClickedCallback(fnName);
      break;
  }
}

function delayed(wait) {
  return new Promise(resolve => {
    setTimeout(resolve, wait);
  });
}

beforeEach(() => {
  jest.clearAllMocks();
});

describe('toggle listeners with debounce', () => {
  const listeners = [onTestClicked, onTestRemoved];
  listeners[0] = debounce(listeners[0], callbackTestFunction, 100);
  listeners[1] = debounce(listeners[1], callbackTestFunction, 100);
  toggleListeners(testObj, listeners, true, 'Test');

  it('onClicked should have one debounce listener', () => {
    expect(testObj.onClicked.listeners[0].isDebounce).toBe(true);
    expect(testObj.onClicked.listeners).toHaveLength(1);
  });

  it('onRemoved should have one debounce listener', () => {
    expect(testObj.onRemoved.listeners[0].isDebounce).toBe(true);
    expect(testObj.onRemoved.listeners).toHaveLength(1);
  });

  it('debounce listener should be triggered once', async () => {
    testObj.sendClick();
    await delayed(0);
    expect(onClicked).toBeCalledTimes(1);
  });

  it('debounce listener callback should be triggered once', async () => {
    testObj.sendClick();
    testObj.sendClick();
    testObj.sendClick();
    await delayed(150);
    expect(onClickedCallback).toBeCalledTimes(1);
    expect(onClickedCallback).toBeCalledWith('onTestClicked');
  });

  it('toggle listeners should be able to remove listener with debounce', async () => {
    toggleListeners(testObj, listeners, false, 'Test');
    expect(testObj.onClicked.listeners).toHaveLength(0);
    testObj.sendClick();
    await delayed(0);
    expect(onClicked).toBeCalledTimes(0);
  });
});

function runTests(listeners) {
  test('toggleListeners without callbacks should add listener without debounce', () => {
    toggleListeners(testObj, listeners, true, 'Test');
    expect(listeners[0].isDebounce).toBeUndefined();
    expect(testObj.onClicked.listeners).toHaveLength(1);
  });

  test('listener without debounce should be triggered for each call', () => {
    testObj.sendClick();
    testObj.sendClick();
    expect(onClicked).toBeCalledTimes(2);
  });

  test('toggleListeners should be able to remove listener without debounce', () => {
    toggleListeners(testObj, listeners, false, 'Test');
    expect(testObj.onClicked.listeners).toHaveLength(0);
    testObj.sendClick();
    expect(onClicked).toBeCalledTimes(0);
  });
}

describe('toggleListeners without debounce', () => {
  const listeners = [onTestClicked];

  runTests(listeners);
});

describe('toggleListeners with extra parameters', () => {
  const listeners = [{
    func: onTestClicked,
    extraParameters: {properties: ['title', 'status']},
  }];

  runTests(listeners);
});

describe('isVersion', () => {
  it('should return false', async () => {
    await expect(isVersion(61)).resolves.toEqual(false);
  });

  it('should return true', async () => {
    await expect(isVersion(59)).resolves.toEqual(true);
  });
});
