/* globals global */
import {getPreferences, resetPreferences, rewireWith} from './_helpers.js';
import {data} from './fixtures/data.js';
import {__RewireAPI__ as titlesRewireAPI} from '../addon/background_scripts/titles.js';

const tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');
let contextMenuRewireAPI, testFunctions;
let preferences;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.debounce = commonRewireAPI.__GetDependency__('debounce');
  global.getDocumentTitle = commonRewireAPI.__GetDependency__('getDocumentTitle');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');
  global.toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');
  global.isVersion = commonRewireAPI.__GetDependency__('isVersion');
  global.tabsTitles = tabsTitles;

  const updateActiveTabRewireAPI = require('../addon/background_scripts/updateActiveTab.js');
  global.updateActiveTab = updateActiveTabRewireAPI.__GetDependency__('updateActiveTab');
  global.updateIcon = updateActiveTabRewireAPI.__GetDependency__('updateIcon');

  const bookmarksRewireAPI = require('../addon/background_scripts/bookmarks.js');
  global.getBookmark = bookmarksRewireAPI.__GetDependency__('getBookmark');

  contextMenuRewireAPI = require('../addon/background_scripts/contextMenus.js');
  testFunctions = rewireWith.bind(null, contextMenuRewireAPI);
});

afterAll(() => {
  resetPreferences();
});

beforeEach(() => {
  jest.clearAllMocks();
});

describe('contextMenus', () => {
  it('onPanelDone should reset title when setDocumentTitle return an error', async () => {
    const onPanelDone = contextMenuRewireAPI.__GetDependency__('onPanelDone');
    tabsTitles.saveTitles(data.page1, data.rename1);

    const originalSetDocumentTitle = global.setDocumentTitle;
    global.setDocumentTitle = jest.fn(() => {
      return {success: false, error: new Error('Unexpected result')};
    });

    const args = {
      tab: {id: 1, title: data.rename1.title},
      stateChanged: true,
      title: 'New User Title',
      permanently: false,
    };
    const responding = await onPanelDone(args);
    expect(responding).toBe(false);
    expect(tabsTitles.getData(1)).toEqual(data.empty);

    global.setDocumentTitle = originalSetDocumentTitle;
  });

  it('browser.browserAction.onClicked should not forward the call', () => {
    return testFunctions('openRenameTabPanel', openRenameTabPanel => {
      // tab with excluded url
      browser.browserAction.onClicked.command({url: 'about:something'});
      expect(openRenameTabPanel).toBeCalledTimes(0);
    });
  });

  it('browser.browserAction.onClicked should forward the call', () => {
    return testFunctions('openRenameTabPanel', openRenameTabPanel => {
      // tab with normal url
      browser.browserAction.onClicked.command({url: 'http://tabmixplus.org'});
      expect(openRenameTabPanel).toBeCalledTimes(1);
    });
  });

  it('browser.browserAction.onClicked should openRenameTabPanel', () => {
    return testFunctions('addPanelListeners', async addPanelListeners => {
      // tab with normal url
      const tab = {id: 1, url: 'http://tabmixplus.org'};
      browser.browserAction.onClicked.command(tab);

      // then 2nd call to browserAction.setPopup is delayed
      await new Promise(resolve => setTimeout(resolve, 200));

      expect(browser.browserAction.setPopup).toBeCalledTimes(2);
      expect(browser.browserAction.setPopup).toBeCalledWith({popup: '/panel/renameTab.html'});
      expect(browser.browserAction.setPopup).toBeCalledWith({popup: ''});
      expect(browser.browserAction.openPopup).toBeCalledTimes(1);

      expect(addPanelListeners).toBeCalledTimes(1);
      expect(addPanelListeners).toBeCalledWith(tab);
    });
  });

  it('changing show_in_context_menu should add/remove menu', () => {
    preferences.show_in_context_menu = false;
    preferences.onChanged.notifyListeners(['show_in_context_menu']);
    expect(browser.contextMenus.remove).toBeCalledTimes(1);

    preferences.show_in_context_menu = true;
    preferences.onChanged.notifyListeners(['show_in_context_menu']);
    expect(browser.contextMenus.create).toBeCalledTimes(1);
  });

  it('onRenameTabMenuShown should call updateMenuItem', () => {
    return testFunctions('updateMenuItem', updateMenuItem => {
      const onRenameTabMenuShown = contextMenuRewireAPI.__GetDependency__('onRenameTabMenuShown');
      onRenameTabMenuShown({}, {discarded: false});
      expect(updateMenuItem).toBeCalledWith(false);

      onRenameTabMenuShown({pageUrl: 'about:something'}, {discarded: false});
      expect(updateMenuItem).toBeCalledWith(false);

      onRenameTabMenuShown({pageUrl: 'http://tabmixplus.org'}, {discarded: false});
      expect(updateMenuItem).toBeCalledWith(true);

      onRenameTabMenuShown({pageUrl: 'http://tabmixplus.org'}, {discarded: true});
      expect(updateMenuItem).toBeCalledWith(false);
    });
  });

  it('updateMenuItem should update menu state', () => {
    const updateMenuItem = contextMenuRewireAPI.__GetDependency__('updateMenuItem');
    updateMenuItem(true);
    expect(browser.contextMenus.update).toBeCalledWith('tabmix-rename-tab', {enabled: true});
    updateMenuItem(false);
    expect(browser.contextMenus.update).toBeCalledWith('tabmix-rename-tab', {enabled: false});
    expect(browser.contextMenus.update).toBeCalledTimes(2);
    expect(browser.contextMenus.refresh).toBeCalledTimes(2);
  });

  it('onRenameTabMenuClicked should not forward the call', () => {
    const onRenameTabMenuClicked = contextMenuRewireAPI.__GetDependency__('onRenameTabMenuClicked');
    const fnList = ['onPanelSateChanged', 'openRenameTabPanel'];
    return testFunctions(fnList, ([onPanelSateChanged, openRenameTabPanel]) => {
      onRenameTabMenuClicked({menuItemId: 'other-menu-id'}, {id: 1});
      expect(onPanelSateChanged).toBeCalledTimes(0);
      expect(openRenameTabPanel).toBeCalledTimes(0);
    });
  });

  it('onRenameTabMenuClicked should forward the call', () => {
    const onRenameTabMenuClicked = contextMenuRewireAPI.__GetDependency__('onRenameTabMenuClicked');
    const fnList = ['onPanelSateChanged', 'openRenameTabPanel'];
    return testFunctions(fnList, ([onPanelSateChanged, openRenameTabPanel]) => {
      onRenameTabMenuClicked({menuItemId: 'tabmix-rename-tab'}, {id: 1, url: 'url-1'});
      expect(onPanelSateChanged).toBeCalledTimes(1);
      expect(onPanelSateChanged).toBeCalledWith({id: 1, url: 'url-1'}, 'opened');
      expect(openRenameTabPanel).toBeCalledTimes(1);
      expect(openRenameTabPanel).toBeCalledWith({id: 1, url: 'url-1'});
    });
  });
});
