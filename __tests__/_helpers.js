import {__RewireAPI__ as storageRewireAPI} from '../addon/background_scripts/storage.js';
const preferences = storageRewireAPI.__GetDependency__('preferences');

let preferencesCopy;

// helpers functions

const deepClone = obj => JSON.parse(JSON.stringify(obj));

export const cloneObject = (obj, extra) => {
  let clone = deepClone(obj);
  if (extra) {
    clone = Object.assign(clone, extra);
  }
  return clone;
};

export const copyObject = (a, b) => {
  Object.entries(a).forEach(([key, value]) => {
    b[key] = value;
  });
};

export const getPreferences = () => {
  preferencesCopy = cloneObject(preferences);
  return preferences;
};

export const resetPreferences = () => {
  if (preferencesCopy) {
    copyObject(preferencesCopy, preferences);
  }
  return preferences;
};

// helper function to mock browser.tabs.query
export function prepareTabs(list) {
  const isArray = Array.isArray(list);
  const n = isArray ? list.length : list;
  const tabs = Array.from({length: n}, (v, i) => ({
    id: i + 1,
    index: i,
    title: `title-${i + 1}`,
    url: isArray ? list[i] : `url-${i + 1}`,
  }));
  browser.tabs.query = jest.fn(({active}) => {
    if (active) {
      const activeTab = tabs.filter(tab => tab.active);
      return Promise.resolve(activeTab.length ? activeTab : [tabs[0]]);
    }
    return Promise.resolve(tabs);
  });
  return tabs;
}

export function rewireWith(rewireAPI, _names, callback) {
  const names = Array.isArray(_names) ? _names : [_names];
  // rewire and get rewired functions
  const functions = names.map(fnName => {
    const {name = fnName, fn = jest.fn()} = fnName;
    rewireAPI.__Rewire__(name, fn);
    return rewireAPI.__GetDependency__(name);
  });

  function reset() {
    names.forEach(fnName => {
      const {name = fnName} = fnName;
      rewireAPI.__ResetDependency__(name);
    });
  }

  const args = Array.isArray(_names) ? functions : functions[0];
  const result = callback(args);

  if (Boolean(result) && typeof result.then == 'function') {
    result.then(reset).catch(reset);
  } else {
    reset();
  }

  return result;
}
