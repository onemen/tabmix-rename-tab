/* globals global */
import {getPreferences, resetPreferences} from './_helpers.js';
import {data} from './fixtures/data.js';
import {__RewireAPI__ as titlesRewireAPI} from '../addon/background_scripts/titles.js';

const messageHelper = require('./message_helpers');

const tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');
let tabUpdateRewireAPI;
let preferences;
let onTabUpdated;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.debounce = commonRewireAPI.__GetDependency__('debounce');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');
  global.toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');
  global.isVersion = commonRewireAPI.__GetDependency__('isVersion');
  global.tabsTitles = tabsTitles;

  const bookmarksRewireAPI = require('../addon/background_scripts/bookmarks.js');
  global.getBookmark = bookmarksRewireAPI.__GetDependency__('getBookmark');

  const sessionsRewireAPI = require('../addon/background_scripts/sessions.js');
  global.getTabValue = sessionsRewireAPI.__GetDependency__('getTabValue');

  tabUpdateRewireAPI = require('../addon/background_scripts/tabUpdated.js');
  onTabUpdated = tabUpdateRewireAPI.__GetDependency__('onTabUpdated');
});

afterAll(() => {
  resetPreferences();
});

afterEach(() => {
  jest.clearAllMocks();
  Object.keys(tabsTitles.data).forEach(id => delete tabsTitles.data[id]);
  tabsTitles.restoredTabs = {};
});

describe('tabUpdated.js - onPreferencesChanged', () => {
  beforeAll(() => {
    // reset sessionTitleFromBookmark and remove all listeners
    tabUpdateRewireAPI.__Rewire__('sessionTitleFromBookmark', false);
    browser.tabs.onUpdated = new messageHelper();
    preferences.title_from_bookmark = false;
  });

  it('sessionTitleFromBookmark should be false on start', () => {
    const sessionTitleFromBookmark = tabUpdateRewireAPI.__GetDependency__('sessionTitleFromBookmark');
    expect(sessionTitleFromBookmark).toBe(false);
  });

  it('toggleOnUpdatedListener should not add listeners on start when title_from_bookmark is of', () => {
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(false);
  });

  it('rename a tab should add listeners', () => {
    tabsTitles.saveTitles(data.page1, data.rename1);
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(true);
    expect(browser.tabs.onUpdated.listeners).toHaveLength(1);

    tabsTitles.saveTitles(data.page2, data.rename2);
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(true);
    // make sure our mock listeners not adding the same listener again
    expect(browser.tabs.onUpdated.listeners).toHaveLength(1);
  });

  it('reset all rename tabs should remove listeners', () => {
    // reset listeners
    browser.tabs.onUpdated = new messageHelper();

    // rename tab with 2 url with different titles
    tabsTitles.saveTitles(data.page1, {title: 'title-1'});
    tabsTitles.saveTitles(data.page2, {title: 'title-2'});
    expect(browser.tabs.onUpdated.listeners).toHaveLength(1);

    tabsTitles.saveTitles(data.page1);
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(true);
    expect(browser.tabs.onUpdated.listeners).toHaveLength(1);

    // only when the last rename data remove the listener should be removed
    tabsTitles.saveTitles(data.page2);
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(false);
    expect(browser.tabs.onUpdated.listeners).toHaveLength(0);
  });

  it('should not remove listeners once title_from_bookmark turn on', () => {
    preferences.title_from_bookmark = true;
    preferences.onChanged.notifyListeners(['title_from_bookmark']);

    const sessionTitleFromBookmark = tabUpdateRewireAPI.__GetDependency__('sessionTitleFromBookmark');
    expect(sessionTitleFromBookmark).toBe(true);
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(true);
    expect(browser.tabs.onUpdated.listeners).toHaveLength(1);

    preferences.title_from_bookmark = false;
    preferences.onChanged.notifyListeners(['title_from_bookmark']);
    expect(browser.tabs.onUpdated.hasListener(onTabUpdated)).toBe(true);
    expect(browser.tabs.onUpdated.listeners).toHaveLength(1);
  });
});

describe('tabUpdated.js - onTabUpdated', () => {
  let setDocumentTitle;
  beforeAll(() => {
    setDocumentTitle = global.setDocumentTitle = jest.fn(() => Promise.resolve());
  });

  function getArguments({status = 'loading', statusUrl, url = 'http://tabmixplus.org', discarded} = {}) {
    // onTabUpdated arguments tabId, changeInfo, tabInfo
    const tabId = 1;
    const changeInfo = {status, url: statusUrl};
    const tabInfo = {id: 1, url, title: 'current title', discarded};
    return [tabId, changeInfo, tabInfo];
  }

  it('should not call setDocumentTitle - excluded url', async () => {
    const args = getArguments({url: 'about:something'});
    await onTabUpdated(...args);
    expect(setDocumentTitle).toBeCalledTimes(0);
  });

  it('should not call setDocumentTitle - pref is off and no user title', async () => {
    preferences.title_from_bookmark = false;
    let args = getArguments();
    await onTabUpdated(...args);
    expect(browser.tabs.sendMessage).toBeCalledTimes(0);
    expect(setDocumentTitle).toBeCalledTimes(0);

    args = getArguments({status: 'loading', statusUrl: 'new url'});
    await onTabUpdated(...args);
    expect(browser.tabs.sendMessage).toBeCalledTimes(1);
    expect(setDocumentTitle).toBeCalledTimes(0);
  });

  it('restoredTabs tab should call setDocumentTitle - only after load completed', async () => {
    tabsTitles.saveTitles(data.page1, data.rename1);

    // set discarded
    let args = getArguments({discarded: true});
    await onTabUpdated(...args);
    expect(setDocumentTitle).toBeCalledTimes(0);

    args = getArguments();
    await onTabUpdated(...args);
    expect(setDocumentTitle).toBeCalledTimes(0);

    await onTabUpdated(...args);
    expect(setDocumentTitle).toBeCalledTimes(0);

    // status: 'complete' should forward to setDocumentTitle
    args = getArguments({status: 'complete'});
    await onTabUpdated(...args);
    expect(setDocumentTitle).toBeCalledTimes(1);
    expect(setDocumentTitle).toBeCalledWith(1, args[2].title, data.rename1.title, args[1]);
  });

  it('should not call setDocumentTitle', async () => {
    preferences.title_from_bookmark = true;
    const args = getArguments();
    await onTabUpdated(...args);
    expect(setDocumentTitle).toBeCalledTimes(1);
    expect(setDocumentTitle).toBeCalledWith(1,
      args[2].title, {id: 'aaa', title: 'Home Page'}, args[1]);
  });
});

