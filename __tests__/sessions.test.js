/* globals global */
import {getPreferences, prepareTabs, resetPreferences, rewireWith} from './_helpers.js';
import {__RewireAPI__ as titlesRewireAPI} from '../addon/background_scripts/titles.js';

const tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');
let sessionsRewireAPI, testFunctions;
let preferences;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.debounce = commonRewireAPI.__GetDependency__('debounce');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');
  global.toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');
  global.isVersion = commonRewireAPI.__GetDependency__('isVersion');
  global.tabsTitles = tabsTitles;

  const bookmarksRewireAPI = require('../addon/background_scripts/bookmarks.js');
  global.getBookmark = bookmarksRewireAPI.__GetDependency__('getBookmark');

  sessionsRewireAPI = require('../addon/background_scripts/sessions.js');
  testFunctions = rewireWith.bind(null, sessionsRewireAPI);
});

afterAll(() => {
  resetPreferences();
});

afterEach(() => {
  jest.clearAllMocks();
  Object.keys(tabsTitles.data).forEach(id => delete tabsTitles.data[id]);
});

describe('sessions', () => {
  let onPreferencesChanged;
  beforeAll(() => {
    onPreferencesChanged = sessionsRewireAPI.__GetDependency__('sessionsPrefsListener');
  });

  it('preferences.onChanged should call onPreferencesChanged', () => {
    return testFunctions('onPreferencesChanged', async preferencesChanged => {
      await preferences.onChanged.addListener(preferencesChanged, true);
      expect(preferencesChanged).toBeCalledTimes(1);
      const changes = [
        'save_with_sessions',
        'show_in_context_menu',
        'title_from_bookmark',
      ];
      expect(preferencesChanged).toBeCalledWith(changes);
    });
  });

  it('onPreferencesChanged should not call updateAllTabsOnPrefsChanged', () => {
    return testFunctions('updateAllTabsOnPrefsChanged', updateAllTabs => {
      sessionsRewireAPI.__Rewire__('firstRun', false);
      onPreferencesChanged(['not_title_from_bookmark']);
      expect(updateAllTabs).toBeCalledTimes(0);
    });
  });

  it('onPreferencesChanged should call updateAllTabsOnPrefsChanged', () => {
    return testFunctions('updateAllTabsOnPrefsChanged', updateAllTabs => {
      onPreferencesChanged(['title_from_bookmark']);
      expect(updateAllTabs).toBeCalledTimes(1);

      updateAllTabs.mockClear();
      sessionsRewireAPI.__Rewire__('firstRun', true);
      onPreferencesChanged([]);
      expect(updateAllTabs).toBeCalledTimes(1);
    });
  });

  it('onPreferencesChanged should not call onSessionPrefChanged', () => {
    return testFunctions('onSessionPrefChanged', onSessionPrefChanged => {
      sessionsRewireAPI.__Rewire__('firstRun', false);
      onPreferencesChanged(['not_save_with_sessions']);
      expect(onSessionPrefChanged).toBeCalledTimes(0);

      sessionsRewireAPI.__Rewire__('firstRun', true);
      onPreferencesChanged(['not_save_with_sessions']);
      expect(onSessionPrefChanged).toBeCalledTimes(0);

      sessionsRewireAPI.__Rewire__('firstRun', true);
      onPreferencesChanged(['save_with_sessions']);
      expect(onSessionPrefChanged).toBeCalledTimes(0);
    });
  });

  it('onPreferencesChanged should call onSessionPrefChanged', () => {
    return testFunctions('onSessionPrefChanged', onSessionPrefChanged => {
      sessionsRewireAPI.__Rewire__('firstRun', false);
      onPreferencesChanged(['save_with_sessions']);
      expect(onSessionPrefChanged).toBeCalledTimes(1);
    });
  });

  it('onPreferencesChanged should not call toggleListeners', () => {
    return testFunctions('toggleListeners', toggleListeners => {
      onPreferencesChanged(['not_save_with_sessions']);
      expect(toggleListeners).toBeCalledTimes(0);
    });
  });

  it('onPreferencesChanged should call toggleListeners', () => {
    const originalToggleListeners = global.toggleListeners;
    const toggleListeners = global.toggleListeners = jest.fn();
    onPreferencesChanged(['save_with_sessions']);
    expect(toggleListeners).toBeCalledTimes(1);
    global.toggleListeners = originalToggleListeners;
  });

  it('should remove/add all listeners when title_from_bookmark pref changes', () => {
    const onTabCreated = sessionsRewireAPI.__GetDependency__('onTabCreated');
    const onTabAttached = sessionsRewireAPI.__GetDependency__('onTabAttached');
    function toggle(state) {
      preferences.save_with_sessions = state;
      preferences.onChanged.notifyListeners(['save_with_sessions']);
      expect(browser.tabs.onCreated.hasListener(onTabCreated)).toBe(state);
      expect(browser.tabs.onAttached.hasListener(onTabAttached)).toBe(state);
    }
    toggle(false);
    toggle(true);
  });

  it('should forward onTabCreated to getTabValue', () => {
    const onTabCreated = sessionsRewireAPI.__GetDependency__('onTabCreated');
    return testFunctions('getTabValue', getTabValue => {
      onTabCreated('some_value', {key: '2nd_value'});
      expect(getTabValue).toBeCalledTimes(1);
      expect(getTabValue).toBeCalledWith('some_value', {key: '2nd_value'});
    });
  });

  it('should forward onTabAttached to updateTabValue', () => {
    const onTabAttached = sessionsRewireAPI.__GetDependency__('onTabAttached');
    return testFunctions('updateTabValue', updateTabValue => {
      onTabAttached('some_value', {key: '2nd_value'});
      expect(updateTabValue).toBeCalledTimes(1);
      expect(updateTabValue).toBeCalledWith('some_value', {key: '2nd_value'});
    });
  });

  it('should call updateTabValue for each tab onSessionPrefChanged', () => {
    const onSessionPrefChanged = sessionsRewireAPI.__GetDependency__('onSessionPrefChanged');
    return testFunctions('updateTabValue', async updateTabValue => {
      prepareTabs(3);
      await onSessionPrefChanged();
      expect(updateTabValue).toBeCalledTimes(3);
      expect(updateTabValue).toBeCalledWith(1);
      expect(updateTabValue).toBeCalledWith(2);
      expect(updateTabValue).toBeCalledWith(3);
    });
  });

  it('updateTabValue should call browser.sessions.setTabValue', () => {
    sessionsRewireAPI.__Rewire__('RENAME_KEY', '_RENAME_KEY_');
    preferences.save_with_sessions = true;
    tabsTitles.saveTitles(prepareTabs(1)[0], {permanently: true, title: 'title'});
    browser.sessions.setTabValue.mockClear();

    const updateTabValue = sessionsRewireAPI.__GetDependency__('updateTabValue');
    updateTabValue(1);

    expect(browser.sessions.setTabValue).toBeCalledTimes(1);
    const state = '{"id":1,"fixedTitle":"title","titles":{}}';
    expect(browser.sessions.setTabValue).toBeCalledWith(1, '_RENAME_KEY_', state);
    expect(browser.sessions.removeTabValue).toBeCalledTimes(0);
  });

  it('updateTabValue should call browser.sessions.removeTabValue', () => {
    sessionsRewireAPI.__Rewire__('RENAME_KEY', '_RENAME_KEY_');
    preferences.save_with_sessions = true;
    const updateTabValue = sessionsRewireAPI.__GetDependency__('updateTabValue');
    updateTabValue(1);

    expect(browser.sessions.setTabValue).toBeCalledTimes(0);
    expect(browser.sessions.removeTabValue).toBeCalledTimes(1);
    expect(browser.sessions.removeTabValue).toBeCalledWith(1, '_RENAME_KEY_');
  });

  it('onTabRemoved should update tabsTitles data and call onReplacingLastTab', () => {
    const onTabRemoved = sessionsRewireAPI.__GetDependency__('onTabRemoved');
    jest.spyOn(tabsTitles, 'removeListeners');
    return testFunctions('onReplacingLastTab', onReplacingLastTab => {
      // preference on
      preferences.save_with_sessions = true;
      tabsTitles.saveTitles(prepareTabs(1)[0], {permanently: true, title: 'title'});

      onTabRemoved(1, {windowId: 1, isWindowClosing: false});

      expect(tabsTitles.data[1]).toBeUndefined();
      expect(onReplacingLastTab).toBeCalledTimes(1);
      expect(onReplacingLastTab).toBeCalledWith({windowId: 1, isWindowClosing: false});
      expect(tabsTitles.removeListeners).toBeCalledTimes(1);

      // preference off
      preferences.save_with_sessions = false;
      onReplacingLastTab.mockClear();
      onTabRemoved(1, {windowId: 1, isWindowClosing: false});
      expect(onReplacingLastTab).toBeCalledTimes(0);
    });
  });

  it('onReplacingLastTab should not call getTabValue', () => {
    const onReplacingLastTab = sessionsRewireAPI.__GetDependency__('onReplacingLastTab');
    return testFunctions('getTabValue', async getTabValue => {
      // window is closing
      prepareTabs(1);
      await onReplacingLastTab({windowId: 1, isWindowClosing: true});

      // there are more than one tab
      prepareTabs(3);
      await onReplacingLastTab({windowId: 1, isWindowClosing: false});
      expect(getTabValue).toBeCalledTimes(0);
    });
  });

  it('onReplacingLastTab should call getTabValue', () => {
    const onReplacingLastTab = sessionsRewireAPI.__GetDependency__('onReplacingLastTab');
    prepareTabs(1);
    return testFunctions('getTabValue', async getTabValue => {
      await onReplacingLastTab({windowId: 1, isWindowClosing: false});
      expect(getTabValue).toBeCalledTimes(1);
      expect(getTabValue).toBeCalledWith({id: 1});
    });
  });
});

describe('sessions updateAllTabsOnPrefsChanged', () => {
  let updateAllTabs;
  beforeAll(() => {
    updateAllTabs = sessionsRewireAPI.__GetDependency__('updateAllTabsOnPrefsChanged');
  });

  it('should not forward the call', () => {
    // all tabs excluded
    prepareTabs(['about:something', 'https://addons.mozilla.org/', 'about:something_else']);
    const fnList = ['getBookmark', 'setDocumentTitle'];
    return testFunctions(fnList, async([mockGetBookmark, setDocumentTitle]) => {
      await updateAllTabs();
      expect(mockGetBookmark).toBeCalledTimes(0);
      expect(setDocumentTitle).toBeCalledTimes(0);
      expect(browser.tabs.query).toBeCalledTimes(1);
      expect(browser.tabs.query).toBeCalledWith({});
    });
  });

  it('should call getTabValue for each tab', () => {
    sessionsRewireAPI.__Rewire__('firstRun', true);
    preferences.save_with_sessions = true;
    prepareTabs(3);

    return testFunctions('getTabValue', async getTabValue => {
      await updateAllTabs();
      expect(getTabValue).toBeCalledTimes(3);
      expect(getTabValue).toBeCalledWith({id: 1, index: 0, title: 'title-1', url: 'url-1'});
      expect(getTabValue).toBeCalledWith({id: 2, index: 1, title: 'title-2', url: 'url-2'});
      expect(getTabValue).toBeCalledWith({id: 3, index: 2, title: 'title-3', url: 'url-3'});

      expect(sessionsRewireAPI.__GetDependency__('firstRun')).toBe(false);
    });
  });

  it('should call browser.sessions.removeTabValue for each tab', async() => {
    sessionsRewireAPI.__Rewire__('firstRun', true);
    sessionsRewireAPI.__Rewire__('RENAME_KEY', '_RENAME_KEY_');
    preferences.save_with_sessions = false;
    prepareTabs(3);

    await updateAllTabs();
    const removeTabValue = browser.sessions.removeTabValue;
    expect(removeTabValue).toBeCalledTimes(3);
    expect(removeTabValue).toBeCalledWith(1, '_RENAME_KEY_');
    expect(removeTabValue).toBeCalledWith(2, '_RENAME_KEY_');
    expect(removeTabValue).toBeCalledWith(3, '_RENAME_KEY_');

    expect(sessionsRewireAPI.__GetDependency__('firstRun')).toBe(false);
  });

  it('should not call getTabValue/removeTabValue', () => {
    sessionsRewireAPI.__Rewire__('firstRun', false);
    prepareTabs(3);

    return testFunctions('getTabValue', async getTabValue => {
      preferences.save_with_sessions = true;
      await updateAllTabs();
      expect(getTabValue).toBeCalledTimes(0);
      expect(browser.sessions.removeTabValue).toBeCalledTimes(0);

      preferences.save_with_sessions = false;
      await updateAllTabs();
      expect(getTabValue).toBeCalledTimes(0);
      expect(browser.sessions.removeTabValue).toBeCalledTimes(0);
    });
  });

  it('should forward the call to setDocumentTitle', async() => {
    // we prepare 5 tabs:
    // one tab should be excluded
    // 2 tabs are with user title
    // 3 tabs are bookmarked see webextension_mock
    const tabs = prepareTabs([
      'http://tabmixplus.org',
      'https://www.babelzilla.org/',
      'about:something',
      'http://tabmixplus.org/forum',
      'http://something.com',
    ]);
    tabsTitles.saveTitles(tabs[1], {permanently: true, title: 'user-title-2'});
    tabsTitles.saveTitles(tabs[3], {permanently: true, title: 'user-title-4'});

    const originalSetDocumentTitle = global.setDocumentTitle;
    const setDocumentTitle = global.setDocumentTitle = jest.fn();

    const originalGetBookmark = global.getBookmark;
    const mockGetBookmark = global.getBookmark = jest.fn(url => originalGetBookmark(url));

    await updateAllTabs();
    expect(browser.tabs.query).toBeCalledTimes(1);
    expect(browser.tabs.query).toBeCalledWith({});

    // should not call getBookmark for tab with user title
    expect(mockGetBookmark).toBeCalledTimes(2);
    expect(mockGetBookmark).toBeCalledWith('http://tabmixplus.org');
    expect(mockGetBookmark).toBeCalledWith('http://something.com');

    // getBookmark is async function we need to wait before we get result
    // for setDocumentTitle
    const results = mockGetBookmark.mock.results.map(result => result.value);
    await Promise.all(results);
    expect(setDocumentTitle).toBeCalledTimes(4);
    expect(setDocumentTitle).toBeCalledWith(1, 'title-1', {id: 'aaa', title: 'Home Page'});
    expect(setDocumentTitle).toBeCalledWith(2, 'title-2', 'user-title-2');
    expect(setDocumentTitle).toBeCalledWith(4, 'title-4', 'user-title-4');
    expect(setDocumentTitle).toBeCalledWith(5, 'title-5', {id: '', title: ''});

    global.setDocumentTitle = originalSetDocumentTitle;
    global.getBookmark = originalGetBookmark;
  });
});

describe('sessions getTabValue', () => {
  let getTabValue;
  beforeAll(() => {
    getTabValue = sessionsRewireAPI.__GetDependency__('getTabValue');
  });

  it('should not call browser.sessions.getTabValue', () => {
    preferences.save_with_sessions = false;
    getTabValue({id: 1, url: 'some_url'});
    preferences.save_with_sessions = true;
    getTabValue({id: 1, url: 'about:something'});
    expect(browser.sessions.getTabValue).toBeCalledTimes(0);
  });

  it('should call creatTitlesObject', async() => {
    preferences.save_with_sessions = true;
    tabsTitles.restoredTabs = {};
    const state = '{"id":1,"fixedTitle":"title","titles":{}}';
    browser.sessions.getTabValue = jest.fn(() => Promise.resolve(state));

    await getTabValue({id: 1, url: 'some_url'});
    expect(tabsTitles.getData(1)).toEqual({id: 1, fixedTitle: 'title', titles: {}});
    expect(tabsTitles.restoredTabs[1]).toBe(true);
  });
});
