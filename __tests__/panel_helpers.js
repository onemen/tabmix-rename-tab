/* globals global */
import {cloneObject, getPreferences, resetPreferences} from './_helpers.js';
import {data} from './fixtures/data.js';

import {__RewireAPI__ as titlesRewireAPI} from '../addon/background_scripts/titles.js';

const tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');

const PATH_TO_PANEL = '../addon/panel/renameTab.js';

let contextMenuRewireAPI;
let preferences;
let panelClosedPromise;

beforeAll(() => {
  // Set up our document body
  document.body.innerHTML = `
    <div id="panel">
      <label id="error-msg"></label>
      <img id="icon" />
      <input id="title-field" type="text">
      <input id="default-title" type="text">
      <input id="permanently" type="checkbox" checked>
      <button id="updateButton" />
      <button id="resetButton" />
    </div>`;

  preferences = getPreferences();
  global.preferences = preferences;
  Object.keys(tabsTitles.data).forEach(id => delete tabsTitles.data[id]);

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.debounce = commonRewireAPI.__GetDependency__('debounce');
  global.getDocumentTitle = commonRewireAPI.__GetDependency__('getDocumentTitle');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');
  global.toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');
  global.isVersion = commonRewireAPI.__GetDependency__('isVersion');
  global.tabsTitles = tabsTitles;

  const updateActiveTabRewireAPI = require('../addon/background_scripts/updateActiveTab.js');
  global.updateActiveTab = updateActiveTabRewireAPI.__GetDependency__('updateActiveTab');
  global.updateIcon = updateActiveTabRewireAPI.__GetDependency__('updateIcon');

  const bookmarksRewireAPI = require('../addon/background_scripts/bookmarks.js');
  global.getBookmark = bookmarksRewireAPI.__GetDependency__('getBookmark');

  contextMenuRewireAPI = require('../addon/background_scripts/contextMenus.js');
});

afterAll(() => {
  resetPreferences();
});

beforeEach(() => {
  panelClosedPromise = new Promise(resolve => {
    window.close = jest.fn(() => resolve());
  });

  jest.resetModules();
  jest.clearAllMocks();
  // reset all listeners
  browser.runtime.onMessage.listeners = [];
});

const prepareMessageFromPanel = (title, permanentlyChecked, expectedRespond) => {
  return {
    ...expectedRespond,
    type: 'rename_panel_done',
    title,
    permanentlyChecked,
  };
};

export function openRenameTabPanel({start, titleFromBookmark, page}) {
  // reset old data
  delete tabsTitles.data[1];

  // set start data
  const startData = data[start] || null;
  if (startData) {
    tabsTitles.creatTitlesObject(1, cloneObject(startData));
  }
  expect(tabsTitles.getData(1)).toEqual(startData || data.empty);

  // turn title_from_bookmark
  preferences.title_from_bookmark = titleFromBookmark;

  // add message listeners
  const addPanelListeners = contextMenuRewireAPI.__GetDependency__('addPanelListeners');
  const asyncOnMessage = addPanelListeners(cloneObject(data[page]));
  expect(browser.runtime.onMessage.addListener).toBeCalledWith(asyncOnMessage);
  expect(browser.runtime.onMessage.hasListener(asyncOnMessage)).toBe(true);

  // mock opening the panel
  require(PATH_TO_PANEL);

  const baseMessage = data.respond[`${page}_start_${start}`];
  const testData = arguments[0];
  const panelData = {asyncOnMessage, baseMessage, testData};

  switch (testData.test || '') {
    case 'keypress':
      return onKeyPressed(panelData, testData.changes);
    case 'error':
      return onError(panelData);
  }

  return renamePage(panelData);
}

async function onKeyPressed({asyncOnMessage}, changes = {}) {
  const sendMessage = browser.runtime.sendMessage.mock;
  await sendMessage.results[0].value;
  expect(browser.runtime.onMessage.hasListener(asyncOnMessage)).toBe(true);

  const updateButtonDisabled = typeof changes.newTitle != 'undefined';
  const $ = id => document.getElementById(id);

  if (updateButtonDisabled) {
    const titleField = $('title-field');
    titleField.value = changes.newTitle;
    titleField.dispatchEvent(new Event('input'));
  }

  window.dispatchEvent(new Event('keypress'));

  if (updateButtonDisabled) {
    expect(browser.runtime.onMessage.hasListener(asyncOnMessage)).toBe(true);
    // browser.runtime.sendMessage({type: 'rename_panel_closed'});
    expect(browser.runtime.sendMessage).not.toBeCalledWith({type: 'rename_panel_closed'});
    $('resetButton').click();
  }

  await panelClosedPromise;
  // panel is closed
  expect(browser.runtime.sendMessage).toBeCalledWith({type: 'rename_panel_closed'});
  expect(browser.runtime.onMessage.hasListener(asyncOnMessage)).toBe(false);
}

async function onError() {
  const sendMessage = browser.runtime.sendMessage.mock;
  await sendMessage.results[0].value;

  // trigger renameTab.js-updateTitle to call onError
  function onMessage(message) {
    switch (message.type) {
      case 'rename_panel_closed':
        if (browser.runtime.onMessage.hasListener(onMessage)) {
          browser.runtime.onMessage.removeListener(onMessage);
        }
        return Promise.resolve('rename_panel_closed');
      case 'rename_panel_done': {
        return Promise.resolve(false);
      }
    }
    return Promise.reject();
  }
  browser.runtime.onMessage.listeners[0] = onMessage;

  const $ = id => document.getElementById(id);
  $('updateButton').click();

  // without an error we expect to have 3 calls and results
  await expect(sendMessage.results[1].value).resolves.toEqual(false);
  expect(sendMessage.results).toHaveLength(2);
  expect(sendMessage.calls).toHaveLength(2);

  // keypress should close the panel after an error
  window.dispatchEvent(new Event('keypress'));
  await panelClosedPromise;

  expect(sendMessage.calls[2][0]).toEqual({type: 'rename_panel_closed'});
  await expect(sendMessage.results[2].value).resolves.toEqual('rename_panel_closed');
}

async function renamePage({asyncOnMessage, baseMessage, testData}) {
  const {changes: {newTitle, checked, buttonId}, expected} = testData;
  const sendMessage = browser.runtime.sendMessage.mock;

  // panel send message when it opened
  const panelOpened = {type: 'rename_panel_opened'};
  expect(browser.runtime.sendMessage).toBeCalledWith(panelOpened);
  const respondMessage = sendMessage.results[0].value;
  await expect(respondMessage).resolves.toEqual(baseMessage);

  // in the panel user can change title and permanently checkbox
  const $ = id => document.getElementById(id);
  const titleField = $('title-field');
  titleField.value = data[newTitle].title;
  titleField.dispatchEvent(new Event('input'));

  const checkbox = $('permanently');
  if (checkbox.checked != checked) {
    checkbox.click();
  }

  $(buttonId).click();

  const messageFromPanel = prepareMessageFromPanel(titleField.value, checked, baseMessage);
  // update data on reset
  if (buttonId == 'resetButton') {
    messageFromPanel.reset = true;
    messageFromPanel.title = messageFromPanel.documentTitle;
  }

  await panelClosedPromise;

  // test calls to browser.runtime.sendMessage
  expect(sendMessage.calls[0][0]).toEqual({type: 'rename_panel_opened'});
  expect(sendMessage.calls[1][0]).toEqual(messageFromPanel);
  expect(sendMessage.calls[2][0]).toEqual({type: 'rename_panel_closed'});

  // test result from browser.runtime.sendMessage
  // we check first result above before it changed
  await expect(sendMessage.results[1].value).resolves.toEqual(true);
  expect(sendMessage.results[2].value).toBeUndefined();

  expect(browser.runtime.onMessage.hasListener(asyncOnMessage)).toBe(false);

  // final result
  expect(tabsTitles.getData(1)).toEqual(data[expected] || data.empty);
}
