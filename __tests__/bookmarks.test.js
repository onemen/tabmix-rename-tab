/* eslint jest/expect-expect: ["error", { "assertFunctionNames": ["expect", "innerUpdateTabTitle"] }] */
/* globals global */
import {getPreferences, prepareTabs, resetPreferences, rewireWith} from './_helpers.js';
import {__RewireAPI__ as titlesRewireAPI} from '../addon/background_scripts/titles.js';

const tabsTitles = titlesRewireAPI.__GetDependency__('tabsTitles');
let bookmarksRewireAPI, getBookmark, testFunctions;
let preferences;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.debounce = commonRewireAPI.__GetDependency__('debounce');
  global.isExcludedUrl = commonRewireAPI.__GetDependency__('isExcludedUrl');
  global.setDocumentTitle = commonRewireAPI.__GetDependency__('setDocumentTitle');
  global.toggleListeners = commonRewireAPI.__GetDependency__('toggleListeners');
  global.tabsTitles = tabsTitles;

  bookmarksRewireAPI = require('../addon/background_scripts/bookmarks.js');
  getBookmark = bookmarksRewireAPI.__GetDependency__('getBookmark');
  testFunctions = rewireWith.bind(null, bookmarksRewireAPI);
});

afterAll(() => {
  resetPreferences();
});

afterEach(() => {
  jest.clearAllMocks();
  Object.keys(tabsTitles.data).forEach(id => delete tabsTitles.data[id]);
});

describe('use bookmark name as tab title', () => {
  const url = 'http://tabmixplus.org';
  it('should call browser.bookmarks.search 2 times for urls without bookmarks with #', async () => {
    preferences.title_from_bookmark = true;
    await getBookmark('http://some.url/#');
    expect(browser.bookmarks.search).toBeCalledTimes(2);
  });
  it('should not return title for existing bookmark when pref is off', async () => {
    preferences.title_from_bookmark = false;
    await expect(getBookmark(url)).resolves.toBe('');
  });
  it('should return bookmark title for existing bookmark', async () => {
    preferences.title_from_bookmark = true;
    expect.assertions(1);
    await expect(getBookmark(url)).resolves.toEqual({
      id: 'aaa',
      title: 'Home Page',
    });
    preferences.title_from_bookmark = false;
  });
});

describe('browser.bookmarks mock listeners', () => {
  let onCreated, onChanged, onRemoved;
  beforeAll(() => {
    onCreated = browser.bookmarks.onCreated.listeners[0];
    onChanged = browser.bookmarks.onChanged.listeners[0];
    onRemoved = browser.bookmarks.onRemoved.listeners[0];
  });

  it('bookmarks listeners should be debounce functions', () => {
    expect(onCreated.isDebounce).toBe(true);
    expect(onChanged.isDebounce).toBe(true);
    expect(onRemoved.isDebounce).toBe(true);
  });

  it('toggle title_from_bookmark pref should remove/add all listeners', () => {
    function toggle(state) {
      preferences.title_from_bookmark = state;
      preferences.onChanged.notifyListeners(['title_from_bookmark']);
      expect(browser.bookmarks.onCreated.hasListener(onCreated)).toBe(state);
      expect(browser.bookmarks.onChanged.hasListener(onChanged)).toBe(state);
      expect(browser.bookmarks.onRemoved.hasListener(onRemoved)).toBe(state);
    }
    toggle(false);
    toggle(true);
  });
});

describe('bookmarks.js listeners', () => {
  let onBookmarkRemoved, onBookmarkChanged, onBookmarkCreated;
  beforeAll(() => {
    onBookmarkRemoved = bookmarksRewireAPI.__GetDependency__('onBookmarkRemoved');
    onBookmarkChanged = bookmarksRewireAPI.__GetDependency__('onBookmarkChanged');
    onBookmarkCreated = bookmarksRewireAPI.__GetDependency__('onBookmarkCreated');
  });

  it('onEndUpdateBatch should forward the call', () => {
    const onEndUpdateBatch = bookmarksRewireAPI.__GetDependency__('onEndUpdateBatch');
    const fnList = ['updateAllTabs', 'onUnspecificChange'];
    return testFunctions(fnList, ([updateAllTabs, onUnspecificChange]) => {
      onEndUpdateBatch('onBookmarkCreated');
      expect(updateAllTabs).toBeCalledTimes(1);
      expect(onUnspecificChange).toBeCalledTimes(0);
      onEndUpdateBatch('onBookmarkChanged');
      expect(updateAllTabs).toBeCalledTimes(2);
      expect(onUnspecificChange).toBeCalledTimes(0);
      onEndUpdateBatch('onBookmarkRemoved');
      expect(updateAllTabs).toBeCalledTimes(2);
      expect(onUnspecificChange).toBeCalledTimes(1);
      onEndUpdateBatch();
      expect(updateAllTabs).toBeCalledTimes(3);
      expect(onUnspecificChange).toBeCalledTimes(1);
    });
  });

  it('onBookmarkRemoved should not forward the call', () => {
    const fnList = ['updateTabsTitleFromBookmark', 'onUnspecificChange'];
    return testFunctions(fnList, ([updateTabsTitle, unspecificChange]) => {
      // preference is off
      preferences.title_from_bookmark = false;
      onBookmarkRemoved(1, {node: {id: 1001, type: 'bookmark'}});
      expect(updateTabsTitle).toBeCalledTimes(0);
      expect(unspecificChange).toBeCalledTimes(0);
      // unknown type should not forward the call
      preferences.title_from_bookmark = true;
      onBookmarkRemoved(1, {node: {type: 'unknown-type'}});
      expect(updateTabsTitle).toBeCalledTimes(0);
      expect(unspecificChange).toBeCalledTimes(0);
    });
  });

  it('onBookmarkRemoved should forward the call', () => {
    const fnList = ['updateTabsTitleFromBookmark', 'onUnspecificChange'];
    return testFunctions(fnList, ([updateTabsTitle, unspecificChange]) => {
      preferences.title_from_bookmark = true;
      const url = 'http://tabmixplus.org';
      // type - bookmark
      onBookmarkRemoved(1, {node: {url, type: 'bookmark'}});
      expect(updateTabsTitle).toBeCalledTimes(1);
      expect(updateTabsTitle).toBeCalledWith({url});
      expect(unspecificChange).toBeCalledTimes(0);
      // type - folder
      onBookmarkRemoved(1, {node: {url, type: 'folder'}});
      expect(updateTabsTitle).toBeCalledTimes(1);
      expect(unspecificChange).toBeCalledTimes(1);
      expect(unspecificChange).toBeCalledWith();
    });
  });

  it('onBookmarkChanged should not forward the call', () => {
    const fnList = ['updateTabsTitleFromBookmark', 'onUnspecificChange'];
    return testFunctions(fnList, async ([updateTabsTitle, unspecificChange]) => {
      // preference is off
      preferences.title_from_bookmark = false;
      onBookmarkChanged(1, {});
      expect(updateTabsTitle).toBeCalledTimes(0);
      expect(unspecificChange).toBeCalledTimes(0);

      // changed node is not a bookmark
      preferences.title_from_bookmark = true;
      browser.bookmarks.get = jest.fn(() => {
        return Promise.resolve([{id: 1001, type: 'not-bookmark'}]);
      });
      await onBookmarkChanged(1, {url: 'some url'});
      expect(updateTabsTitle).toBeCalledTimes(0);
      expect(unspecificChange).toBeCalledTimes(0);
    });
  });

  it('onBookmarkChanged should forward the call', () => {
    const fnList = ['updateTabsTitleFromBookmark', 'onUnspecificChange'];
    return testFunctions(fnList, async ([updateTabsTitle, unspecificChange]) => {
      // preference is on and the changed id is bookmark
      const bookmark = {
        type: 'bookmark',
        url: 'some url',
        title: 'new title',
      };
      preferences.title_from_bookmark = true;
      browser.bookmarks.get = jest.fn(id => {
        return Promise.resolve([{id, ...bookmark}]);
      });
      // url changed
      await onBookmarkChanged(1001, {url: 'some url'});
      expect(unspecificChange).toBeCalledTimes(1);
      expect(unspecificChange).toBeCalledWith(1001);
      expect(updateTabsTitle).toBeCalledTimes(1);
      expect(updateTabsTitle).lastCalledWith({id: 1001, ...bookmark});
      // title changed
      await onBookmarkChanged(2002, {title: 'new title'});
      expect(unspecificChange).toBeCalledTimes(1);
      expect(updateTabsTitle).toBeCalledTimes(2);
      expect(updateTabsTitle).lastCalledWith({id: 2002, ...bookmark});
    });
  });

  it('onBookmarkCreated should not forward the call', () => {
    return testFunctions('updateTabsTitleFromBookmark', fnName => {
      // not bookmark
      preferences.title_from_bookmark = true;
      onBookmarkCreated(1, {id: 1001, type: 'not-bookmark'});
      // preference off
      preferences.title_from_bookmark = false;
      onBookmarkCreated(1, {id: 1001, type: 'bookmark'});
      expect(fnName).toBeCalledTimes(0);
    });
  });

  it('onBookmarkCreated should forward the call', () => {
    return testFunctions('updateTabsTitleFromBookmark', fnName => {
      preferences.title_from_bookmark = true;
      onBookmarkCreated(1, {id: 1001, type: 'bookmark'});
      expect(fnName).toBeCalledTimes(1);
      expect(fnName).toBeCalledWith({id: 1001, type: 'bookmark'});
    });
  });
});

describe('updateTabsTitleFromBookmark', () => {
  let updateTitle;
  beforeAll(() => {
    updateTitle = bookmarksRewireAPI.__GetDependency__('updateTabsTitleFromBookmark');
  });
  const bookmark = {
    id: 1001,
    type: 'bookmark',
    url: 'some url',
    title: 'new title',
  };

  it('should not forward the call', () => {
    // we get 2 tabs with the same url
    const tabs = prepareTabs(2);

    // set user title for the same tabs
    tabsTitles.saveTitles(tabs[0], {permanently: true, title: 'title'});
    tabsTitles.saveTitles(tabs[1], {permanently: true, title: 'title'});

    return testFunctions('setDocumentTitle', async setDocumentTitle => {
      // call updateTabsTitleFromBookmark with excluded url
      await updateTitle({...bookmark, url: 'about:something'});
      expect(setDocumentTitle).toBeCalledTimes(0);

      // all tab for queried url have user title
      await updateTitle({...bookmark, url: 'url'});
      expect(setDocumentTitle).toBeCalledTimes(0);
      expect(browser.tabs.query).toBeCalledWith({url: 'url'});
    });
  });

  it('should forward the call', async () => {
    // we get 5 tabs with the same url
    const tabs = prepareTabs(5);

    // set user title for the same tabs
    tabsTitles.saveTitles(tabs[1], {permanently: true, title: 'title'});
    tabsTitles.saveTitles(tabs[3], {permanently: true, title: 'title'});

    const originalSetDocumentTitle = global.setDocumentTitle;
    const setDocumentTitle = global.setDocumentTitle = jest.fn();

    // only tabs without user title should call updateTabsTitleFromBookmark
    const data = {...bookmark, url: 'url'};
    await updateTitle(data);
    // tabs with ids 2 and 4 have with user title
    expect(setDocumentTitle).toBeCalledTimes(3);
    expect(setDocumentTitle).toBeCalledWith(1, 'title-1', data);
    expect(setDocumentTitle).toBeCalledWith(3, 'title-3', data);
    expect(setDocumentTitle).toBeCalledWith(5, 'title-5', data);
    expect(browser.tabs.query).toBeCalledWith({url: 'url'});

    global.setDocumentTitle = originalSetDocumentTitle;
  });
});

describe('updateAllTabs', () => {
  let updateAllTabs;
  beforeAll(() => {
    updateAllTabs = bookmarksRewireAPI.__GetDependency__('updateAllTabs');
  });

  it('should not forward the call', () => {
    // all tabs excluded or with user title
    const tabs = prepareTabs(['about:something', 'http://tabmixplus.org', 'https://www.babelzilla.org/']);
    tabsTitles.saveTitles(tabs[1], {permanently: true, title: 'title'});
    tabsTitles.saveTitles(tabs[2], {permanently: true, title: 'title'});

    const fnList = ['getBookmark', 'setDocumentTitle'];
    return testFunctions(fnList, async ([mockGetBookmark, setDocumentTitle]) => {
      await updateAllTabs();
      expect(mockGetBookmark).toBeCalledTimes(0);
      expect(setDocumentTitle).toBeCalledTimes(0);
      expect(browser.tabs.query).toBeCalledTimes(1);
      expect(browser.tabs.query).toBeCalledWith({});
    });
  });

  it('should forward the call', () => {
    // 2 tabs are bookmarked see webextension_mock
    prepareTabs(['about:something', 'http://tabmixplus.org', 'https://www.babelzilla.org/']);
    const originalSetDocumentTitle = global.setDocumentTitle;
    const setDocumentTitle = global.setDocumentTitle = jest.fn();
    const original = getBookmark;
    const mockData = {name: 'getBookmark', fn: jest.fn(url => original(url))};
    const fnList = [mockData];
    return testFunctions(fnList, async ([mockGetBookmark]) => {
      await updateAllTabs();
      expect(browser.tabs.query).toBeCalledTimes(1);
      expect(browser.tabs.query).toBeCalledWith({});

      expect(mockGetBookmark).toBeCalledTimes(2);
      expect(mockGetBookmark).toBeCalledWith('http://tabmixplus.org');
      expect(mockGetBookmark).toBeCalledWith('https://www.babelzilla.org/');

      // getBookmark is async function we need to wait before we get result
      // for setDocumentTitle
      const results = mockGetBookmark.mock.results.map(result => result.value);
      await Promise.all(results);
      expect(setDocumentTitle).toBeCalledTimes(2);
      expect(setDocumentTitle).toBeCalledWith(2, 'title-2', {id: 'aaa', title: 'Home Page'});
      expect(setDocumentTitle).toBeCalledWith(3, 'title-3', {id: 'ccc', title: 'Provide localizations'});

      global.setDocumentTitle = originalSetDocumentTitle;
    });
  });
});

describe('onUnspecificChange and updateTabTitle', () => {
  let updateTabTitle;
  beforeAll(() => {
    updateTabTitle = bookmarksRewireAPI.__GetDependency__('updateTabTitle');
  });
  it('onUnspecificChange should forward the call', () => {
    const onUnspecificChange = bookmarksRewireAPI.__GetDependency__('onUnspecificChange');
    // 2 tabs are bookmarked see webextension_mock
    prepareTabs(['url-1', 'url-2']);
    return testFunctions('updateTabTitle', async mockUpdateTabTitle => {
      await onUnspecificChange('1001');
      expect(browser.tabs.query).toBeCalledTimes(1);
      expect(browser.tabs.query).toBeCalledWith({});

      expect(mockUpdateTabTitle).toBeCalledTimes(2);
      expect(mockUpdateTabTitle).toBeCalledWith({id: 1, index: 0, title: 'title-1', url: 'url-1'}, '1001');
      expect(mockUpdateTabTitle).toBeCalledWith({id: 2, index: 1, title: 'title-2', url: 'url-2'}, '1001');
    });
  });

  it('updateTabTitle should not forward the call', async () => {
    // excluded url
    await updateTabTitle({url: 'addons.mozilla.org'});

    // tab with user title
    const tabs = prepareTabs(['http://tabmixplus.org']);
    tabsTitles.saveTitles(tabs[0], {permanently: true, title: 'title'});
    await updateTabTitle(tabs[0], '1001');

    expect(browser.tabs.sendMessage).toBeCalledTimes(0);
  });

  function innerUpdateTabTitle(bookmarkId, tabBookmarkId, calledTimes = 0) {
    const originalSetDocumentTitle = global.setDocumentTitle;
    const setDocumentTitle = global.setDocumentTitle = jest.fn();
    // mock getBookmark
    const original = getBookmark;
    const mockData = {name: 'getBookmark', fn: jest.fn(url => original(url))};
    const fnList = [mockData];
    // mock browser.tabs.sendMessage
    browser.tabs.sendMessage = jest.fn(() => Promise.resolve(tabBookmarkId));
    return testFunctions(fnList, async ([mockGetBookmark]) => {
      const tabs = prepareTabs(['http://tabmixplus.org/forum']);
      await updateTabTitle(tabs[0], bookmarkId);

      expect(browser.tabs.sendMessage).toBeCalledTimes(1);
      expect(browser.tabs.sendMessage).toBeCalledWith(tabs[0].id, {type: 'get_bookmark_id'});
      await expect(browser.tabs.sendMessage.mock.results[0].value).resolves.toEqual(tabBookmarkId);

      expect(mockGetBookmark).toBeCalledTimes(calledTimes);
      expect(setDocumentTitle).toBeCalledTimes(calledTimes);
      if (calledTimes) {
        expect(setDocumentTitle).toBeCalledWith(1, 'title-1', {id: 'bbb', title: 'Forum'});
      }

      global.setDocumentTitle = originalSetDocumentTitle;
    });
  }

  it('updateTabTitle should not forward the call when tab was not bookmarked', () => {
    // expecting respond from sendMessage to be undefined
    return innerUpdateTabTitle('1001');
  });

  it('updateTabTitle should not forward the call when tab was bookmarked with different id', () => {
    // expecting respond to sendMessage to be un-match bookmark id
    return innerUpdateTabTitle('1001', '2002');
  });

  it('updateTabTitle should forward the call when tab was bookmarked', async () => {
    // here we test 2 cases:
    // 1. updateTabTitle was called without bookmarkId and the tab was bookmarked
    // 2. updateTabTitle was called with bookmarkId and the tab url bookmarked with the same id
    await innerUpdateTabTitle(undefined, '2002', 1);

    return innerUpdateTabTitle('1001', '1001', 1);
  });
});
