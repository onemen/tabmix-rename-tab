/**
* @jest-environment jsdom
*/
/* globals global */

import {getPreferences, prepareTabs, resetPreferences, rewireWith} from './_helpers.js';

document.body.innerHTML = `
    <section class="panel-section-list">
      <input id="title_from_bookmark" type="checkbox" checked="true"/>
      <input id="save_with_sessions" type="checkbox" checked="true"/>
      <input id="show_in_context_menu" type="checkbox" checked="true"/>
      <input id="not_preferences" type="not_checkbox" checked="true"/>
      <button class="contribute"></button>
    </section>`;

let optionsRewireAPI, testFunctions;
let preferences;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;

  const commonRewireAPI = require('../addon/background_scripts/common.js');
  global.loadLocalStrings = commonRewireAPI.__GetDependency__('loadLocalStrings');

  optionsRewireAPI = require('../addon/options/options.js');
  testFunctions = rewireWith.bind(null, optionsRewireAPI);
});

afterAll(() => {
  resetPreferences();
});

beforeEach(() => {
  jest.clearAllMocks();
});

describe('options', () => {
  it('panel-section-list should have an eventListener', () => {
    return testFunctions('savePreferences', savePreferences => {
      const checkbox = document.getElementById('title_from_bookmark');
      checkbox.click();
      expect(savePreferences).toBeCalledTimes(1);
      expect(savePreferences).toBeCalledWith(checkbox);
    });
  });

  it('savePreferences should not call storage with unknown type', () => {
    const savePreferences = optionsRewireAPI.__GetDependency__('savePreferences');
    const consoleError = console.error;
    console.error = jest.fn();
    const msg = obj => `need to update option UI for ${obj.id} with type ${obj.type}`;
    const testArgs = [
      {id: 'title_from_bookmark', type: '"type is not checkbox"'},
      {id: 'not_preference', type: 'checkbox'},
    ];

    savePreferences(testArgs[0]);
    expect(console.error).toBeCalledTimes(1);
    expect(console.error).lastCalledWith(msg(testArgs[0]));

    savePreferences(testArgs[1]);
    expect(console.error).toBeCalledTimes(2);
    expect(console.error).lastCalledWith(msg(testArgs[1]));

    expect(browser.storage.local.set).toBeCalledTimes(0);
    console.error = consoleError;
  });

  it('savePreferences should call storage with the right value', () => {
    const checkbox = document.getElementById('title_from_bookmark');
    checkbox.checked = true;
    // click on checkbox toggle checked property
    checkbox.click();
    expect(browser.storage.local.set).toBeCalledTimes(1);
    expect(browser.storage.local.set).toBeCalledWith({title_from_bookmark: false});
    checkbox.click();
    expect(browser.storage.local.set).toBeCalledWith({title_from_bookmark: true});
  });

  it('preferences.onChanged should call updateUI', () => {
    const consoleError = console.error;
    console.error = jest.fn();
    const prefs = [
      'save_with_sessions',
      'show_in_context_menu',
      'title_from_bookmark',
    ];
    prefs.forEach(pref => (preferences[pref] = false));
    preferences.onChanged.notifyListeners(prefs);
    prefs.forEach(pref => {
      const checkbox = document.getElementById(pref);
      expect(checkbox.checked).toBe(false);
    });
    expect(console.error).toBeCalledTimes(0);

    preferences.onChanged.notifyListeners(['not_preferences']);
    expect(console.error).toBeCalledTimes(1);
    const msg = 'need to update option UI for not_preferences with type not_checkbox';
    expect(console.error).toBeCalledWith(msg);

    console.error = consoleError;
  });
});

describe('i18n', () => {
  beforeAll(() => {
    // Set up our document body
    document.body.innerHTML = `
      <div id="panel">
        <label id="label1" i18n="message_1 [arg1,arg2] extra2"></label>
        <label id="label2" i18n="message_2"></label>
        <label></label>
        <label id="label3" i18n="message_3 extra1"></label>
      </div>`;
  });

  it('loadLocalStrings should get arguments with getMessage', () => {
    browser.i18n.getMessage = jest.fn((a, b = []) => `result_${a}:${b.join('')}`);
    const loadStrings = optionsRewireAPI.__GetDependency__('loadStrings');
    loadStrings(document);
    expect(browser.i18n.getMessage).toBeCalledTimes(5);
    expect(browser.i18n.getMessage.mock.calls[0][0]).toEqual('arg1');
    expect(browser.i18n.getMessage.mock.calls[1][0]).toEqual('arg2');
    expect(browser.i18n.getMessage.mock.calls[2][0]).toEqual('message_1', ['result_arg1:', 'result_arg2:']);
    expect(browser.i18n.getMessage.mock.calls[3][0]).toEqual('message_2');
    expect(browser.i18n.getMessage.mock.calls[4][0]).toEqual('message_3');

    const $ = id => document.getElementById(id);
    expect($('label1').textContent).toEqual('result_message_1:result_arg1:result_arg2:extra2');
    expect($('label2').textContent).toEqual('result_message_2:');
    expect($('label3').textContent).toEqual('result_message_3:extra1');
  });
});

describe('openContributeTab', () => {
  it('should open new tab after the current tab', async() => {
    const url = 'http://tabmixplus.org/support/contribute/contribute.html';
    const tabs = prepareTabs(3);
    tabs[1].active = true;
    const openContributeTab = optionsRewireAPI.__GetDependency__('openContributeTab');
    await openContributeTab();
    expect(browser.tabs.query).toBeCalledTimes(1);
    expect(browser.tabs.create).toBeCalledTimes(1);
    expect(browser.tabs.create).toBeCalledWith({url, index: 2});
  });
});

