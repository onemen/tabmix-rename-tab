export const data = {
  page1: {
    id: 1,
    url: 'http://tabmixplus.org',
    title: 'Tab Mix Plus',
    favIconUrl: '../../icons/tabmix-32.png',
  },
  rename1: {
    permanently: true,
    title: 'User Title 1',
  },
  expected1: {
    id: 1,
    fixedTitle: 'User Title 1',
    titles: {},
  },
  page2: {
    id: 1,
    url: 'http://tabmixplus.org/forum',
    title: 'Tab Mix Plus forum',
    favIconUrl: '../../icons/tabmix-32.png',
  },
  rename2: {
    permanently: true,
    title: 'User Title 2',
  },
  expected2: {
    id: 1,
    fixedTitle: 'User Title 2',
    titles: {},
  },
  expected3: {
    id: 1,
    fixedTitle: '',
    titles: {'http://tabmixplus.org': 'User Title 1'},
  },
  expected4: {
    id: 1,
    fixedTitle: '',
    titles: {
      'http://tabmixplus.org': 'User Title 1',
      'http://tabmixplus.org/forum': 'User Title 2',
    },
  },
  empty: {
    fixedTitle: '',
    titles: {},
  },
};

// use for test respond messages from 'rename_panel_opened'
const respond = {
  page1_start_null: {
    type: 'init_state_for_panel',
    permanently: false,
    permanentlyChecked: true,
    title: data.page1.title,
    documentTitle: 'documentTitle',
    currentUserTitle: '',
    modified: false,
  },
  page1_start_expected2: {
    type: 'init_state_for_panel',
    permanently: true,
    permanentlyChecked: true,
    title: data.page1.title,
    documentTitle: 'documentTitle',
    currentUserTitle: data.rename2.title,
    modified: true,
  },
  page2_start_expected2: {
    type: 'init_state_for_panel',
    permanently: true,
    permanentlyChecked: true,
    title: data.page2.title,
    documentTitle: 'documentTitle',
    currentUserTitle: data.rename2.title,
    modified: true,
  },
  page2_start_expected3: {
    type: 'init_state_for_panel',
    permanently: false,
    permanentlyChecked: false,
    title: data.page2.title,
    documentTitle: 'documentTitle',
    currentUserTitle: '',
    modified: false,
  },
  page2_start_expected4: {
    type: 'init_state_for_panel',
    permanently: false,
    permanentlyChecked: false,
    title: data.page2.title,
    documentTitle: 'documentTitle',
    currentUserTitle: data.rename2.title,
    modified: true,
  },
};

data.respond = respond;
