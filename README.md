# Tab Mix - Rename tab

## About this extension

Provides a simple interface to change tab title and option to use bookmark name as tab title.

This extensions is part of the new **Tab Mix WebExtension** collection that will eventually replace the legacy ***Tab Mix Plus*** extensions.

### User Interface

* This extension adds a button to the main toolbar.  
Click the button to open **Edit tab title** panel to change the active tab title.

* **Rename Tab** menu item is added to the tab context menu.  
You can use this menu to edit the title of background or current tab.

Due to Firefox restriction the extension can not edit title for 'about:' pages and for 'addons.mozilla.org' pages.

---

### Options

To change options go to the extension tab in **Add-ons** (*about:addons*).  
All options are on by default.

* **Use bookmark name as tab title**  
Turn this option on to automatically replace the title for bookmarked pages when the bookmark name.

* **Save user title between sessions**  
When this option is on, the extensions uses Sessions API to save the title you edit between sessions.

* **Show '_Rename Tab_' in tab context menu**  
When this option is on the extension adds the Rename Tab menu item to tab context menu.

---

### Permissions
List of Permissions the extension use:

* **<all_urls>**  
Allow the extension to change document.title in all pages.

* **activeTab and tabs**  
Allow the extension to listen to changes in tabs address, active tab, etc.

* **bookmarks**  
Allow the extension interact with and manipulate the browser's bookmarking system.  
needed for: _Use bookmark name as tab title_.

* **contextMenus**  
Allow the extension add items to the browser's context menu.  
needed for: _Show '**Rename Tab**' in tab context menu_.

* **sessions**  
Enable an extension to store additional state associated with a tab.  
needed for: _Save user title between sessions_.

* **storage**  
Enables extensions to store and retrieve data, and listen for changes to stored items.  
needed for storing user options.

---

**Icons** from Icon Fonts - http://www.onlinewebfonts.com/icon, License: CC BY 3.0
